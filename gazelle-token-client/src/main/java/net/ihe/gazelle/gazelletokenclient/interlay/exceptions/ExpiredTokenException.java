package net.ihe.gazelle.gazelletokenclient.interlay.exceptions;

public class ExpiredTokenException extends RuntimeException {
    public ExpiredTokenException(String s) {
        super(s);
    }
}
