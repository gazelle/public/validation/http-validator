package net.ihe.gazelle.gazelletokenclient.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class IdentityToken {

    private String value;

    private String username;

    private String organization;

    private Date creationDate;

    private Date expirationDate;

    private List<String> roles = new ArrayList<>();

    public IdentityToken() {
    }

    public IdentityToken(IdentityTokenBuilder builder) {
        this.value = builder.value;
        this.username = builder.username;
        this.organization = builder.organization;
        this.creationDate = builder.creationDate;
        this.expirationDate = builder.expirationDate;
        this.roles = builder.roles;
    }

    public static IdentityTokenBuilder builder() {
        return new IdentityTokenBuilder();
    }

    public static class IdentityTokenBuilder {
        private String value;
        private String username;
        private String organization;
        private Date creationDate;
        private Date expirationDate;
        private List<String> roles;

        public IdentityTokenBuilder withValue(final String value) {
            this.value = value;
            return this;
        }

        public IdentityTokenBuilder withUsername(final String username) {
            this.username = username;
            return this;
        }

        public IdentityTokenBuilder withOrganization(final String organization) {
            this.organization = organization;
            return this;
        }

        public IdentityTokenBuilder withCreationDate(final Date creationDate) {
            this.creationDate = creationDate;
            return this;
        }

        public IdentityTokenBuilder withExpirationDate(final Date expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public IdentityTokenBuilder withRoles(final List<String> roles) {
            this.roles = new ArrayList<>(roles);
            return this;
        }

        public IdentityToken build() {
            return new IdentityToken(this);
        }
    }

    public String getValue() {
        return value;
    }

    public String getUsername() {
        return username;
    }

    public String getOrganization() {
        return organization;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public List<String> getRoles() {
        return new ArrayList<>(this.roles);
    }

    public boolean isAdmin() {
        return roles.contains("admin_role");
    }
}