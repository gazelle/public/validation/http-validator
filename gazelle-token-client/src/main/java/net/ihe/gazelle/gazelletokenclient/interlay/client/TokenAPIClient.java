package net.ihe.gazelle.gazelletokenclient.interlay.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import net.ihe.gazelle.gazelletokenclient.application.AuthenticationClient;
import net.ihe.gazelle.gazelletokenclient.domain.IdentityToken;
import net.ihe.gazelle.gazelletokenclient.interlay.exceptions.ExpiredTokenException;
import net.ihe.gazelle.gazelletokenclient.interlay.exceptions.InvalidTokenException;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.client.jaxrs.internal.ResteasyClientBuilderImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@ApplicationScoped
public class TokenAPIClient implements AuthenticationClient {

    private static final Logger LOG = LoggerFactory.getLogger(TokenAPIClient.class);
    private static final String TOKEN_ENDPOINT = "/rest/v1/authn/user/";

    @Override
    public IdentityToken requestGazelleTokenService(String endpoint, String token) throws InvalidTokenException, ExpiredTokenException {
        assertNonNullEndpointAndToken(endpoint, token);
        ResteasyClient tokenClient = new ResteasyClientBuilderImpl().build();
        ResteasyWebTarget target = tokenClient.target(endpoint + TOKEN_ENDPOINT + token);
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        return getIdentityTokenFromResponse(response);
    }

    private void assertNonNullEndpointAndToken(String endpoint, String token) {
        if (endpoint == null || token == null ) {
            LOG.debug("Both endpoint and token are required");
            throw new InvalidTokenException("Bad Request URI");
        }
    }

    private IdentityToken getIdentityTokenFromResponse(Response response) {
        try {
            int status = response.getStatus();

            if (status == Response.Status.BAD_REQUEST.getStatusCode() || status == Response.Status.NOT_FOUND.getStatusCode()) {
                throw new InvalidTokenException("Bad request or Not found");
            } else if (status == Response.Status.GONE.getStatusCode()) {
                throw new ExpiredTokenException("Token expired");
            } else if (status == Response.Status.OK.getStatusCode()) {
                ObjectMapper mapper = new ObjectMapper();
                String tokenString = response.readEntity(String.class);
                return mapper.readValue(tokenString, IdentityToken.class);
            } else {
                throw new InvalidTokenException("Unknown error");
            }
        } catch (IOException e) {
            LOG.debug(e.getMessage(), e);
            throw new InvalidTokenException("Fail to convert to Json");
        }
    }
}