package domain;

import mock.MockDomainToken;
import net.ihe.gazelle.gazelletokenclient.domain.IdentityToken;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class IdentityTokenTest {

    @Test
    void createAdminTokenTest() {
        IdentityToken identityToken = MockDomainToken.createAdminMockIdentityToken();
        assertEquals("token", identityToken.getValue());
        assertEquals(1676634496000L, identityToken.getCreationDate().getTime());
        assertEquals(2213092096000L, identityToken.getExpirationDate().getTime());
        assertEquals("KEREVAL", identityToken.getOrganization());
        assertEquals("username", identityToken.getUsername());
        assertEquals(1, identityToken.getRoles().size());
        assertTrue(identityToken.isAdmin());
    }
}
