package mock;

import net.ihe.gazelle.gazelletokenclient.domain.IdentityToken;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MockDomainToken {

    private static final Date CREATION_DATE = new Date(1676634496000L);
    private static final Date EXPIRATION_DATE = new Date(2213092096000L);

    public static IdentityToken createAdminMockIdentityToken() {
        List<String> roles = new ArrayList<>();
        roles.add("admin_role");
        return IdentityToken.builder()
                .withValue("token")
                .withCreationDate(CREATION_DATE)
                .withExpirationDate(EXPIRATION_DATE)
                .withOrganization("KEREVAL")
                .withUsername("username")
                .withRoles(roles)
                .build();
    }

    public static IdentityToken createNotAdminMockIdentityToken() {
        List<String> roles = new ArrayList<>();
        roles.add("vendor_role");
        return IdentityToken.builder()
                .withValue("token")
                .withCreationDate(CREATION_DATE)
                .withExpirationDate(EXPIRATION_DATE)
                .withOrganization("KEREVAL")
                .withUsername("username")
                .withRoles(roles)
                .build();
    }

}
