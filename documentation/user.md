---
title:  User Manual
subtitle: HTTP Validator
author: Clément LAGORCE
function: Software Engineer
date: 21=2/08/2023
toolversion: 0.3.0
version: 1_03
status: Reviewed
reference: KER1-MAN-IHE-HTTP-VALIDATOR_USER-1_03
customer: IHE-EUROPE
---    

# Introduction
This application has been developed with the purpose of validating any HTTP Message (Request or Response) in a full raw String format.
- For an HTTP Request : The message is a String containing the HTTP Method, URI, Query parameters, HTTP Version and HTTP Headers.
- For an HTTP Response : The message is a String containing the HTTP Version, the Status Code, the Reason Phrase and the HTTP Headers.

Every message shall be formatted as specified in the [RFC 2616](https://www.rfc-editor.org/rfc/rfc2616) of the HTTP Protocol. This includes the double CRLF at the end of the HTTP Headers.

As we can see in [Section 2.2](https://www.rfc-editor.org/rfc/rfc2616#section-2.2), [Section 5](https://www.rfc-editor.org/rfc/rfc2616#section-5) and [Section 6](https://www.rfc-editor.org/rfc/rfc2616#section-6), the HTTP message shall be provided to the tool with the following format :
~~~text
CRLF = CR LF
CR = <US-ASCII CR, carriage return (13)>
LF = <US-ASCII LF, linefeed (10)>
SP = <US-ASCII SP, space (32)>

Request = Method SP Request-URI SP HTTP-Version CRLF          
          *(( general-header
           | request-header
           | entity-header ) CRLF)
          CRLF
          [ message-body ]
          
Response = HTTP-Version SP Status-Code SP Reason-Phrase CRLF
           *(( general-header
            | response-header
            | entity-header ) CRLF)
           CRLF
           [ message-body ]
~~~

This tool provides a way to validate automatically any HTTP Message from any standard or profile that uses RestFul Web Services.
The messages are validated against a configuration file called Validation Profile formatted in Json.

## Purpose of the document
This document is the Customer user manual for the Gazelle HTTP Validator module.

## Scope of the document
The scope of this document is limited to the usage of Gazelle HTTP Validator, how to create a Validation Profile and how to perform an HTTP Message validation.

How to install, configure and maintain the tool is described in the Installation manual.

## HTTP Validator Engine
This part of the application is the validation Engine that will validate the HTTP message provided to the validator. It is directly connected to EVS Client.

### EVSClient Validation
The other way to perform HTTP message validation is by using EVSClient. A configuration shall be done in EVSClient in the same platform where HTTP-Validator is deployed.
Once EVS is configured, this menu should appear at the top bar :
![](./images/ValidationMenu.png)
The Validation Report is displayed in a similar manner as it is for other validators.
Below is the result of the same HTTP message against the same Validation Profile as for the Rest API :
![](./images/ValidationResultEVS.png)

# Tools needed

## HTTP Validator Service

### Sources and libraries
The sources are available on Inria’s Gitlab in the Gazelle project : _https://gitlab.inria.fr/gazelle/applications/test-execution/validator/http-validator_

Released binaries are available in the Gazelle Nexus repository at : _https://gazelle.ihe.net/nexus/#nexus-search;gav~~http-validator_

### Issue tracker
Issues and features requests shall be entered through the issue bug tracking system of the Gazelle platform, at _https://gazelle.ihe.net/jira/browse/HTTP_VAL_.

## EVSClient

### Sources and libraries
The sources are available on Inria’s Gitlab in the Gazelle project : _https://gitlab.inria.fr/gazelle/applications/test-execution/validator/EVSClient_.

Released binaries are available in the Gazelle Nexus repository at : _https://gazelle.ihe.net/nexus/index.html#nexus-search;gav~~EVSClient_

### Issue tracker
For an issue specific to the gazelle-x-validation module, please use the issue tracker at _https://gazelle.ihe.net/jira/browse/HTTP_VAL_. For an issue specific to the EVS Client, you can use _https://gazelle.ihe.net/jira/browse/EVSCLT_.

## Gazelle Token Service

### Sources and libraries
The sources are available on Inria’s Gitlab in the Gazelle project : _https://gitlab.inria.fr/gazelle/applications/core/gazelle-token_.

Released binaries are available in the Gazelle Nexus repository at : _https://gazelle.ihe.net/nexus/index.html#nexus-search;quick~gazelle-token_.