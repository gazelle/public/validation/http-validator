---
title:  User Manual
subtitle: HTTP Validator
author: Clément LAGORCE
function: Software Engineer
date: 22/08/2023
toolversion: 0.3.0
version: 1_03
status: Reviewed
reference: KER1-MAN-IHE-HTTP-VALIDATOR_USER-1_03
customer: IHE-EUROPE
---    

# Purpose

Here is a guide to help you install the IUA-Simulator.

# Deployment

## Minimal requirements

* Debian squeeze or ubuntu 12.04 64bits or higher with an Internet access.
* Java virtual machine : JDK 17
* Application server : Wildfly 27

To install those requirements you can refer to the documentation of installation of Wildfly : [*General Requirements Wildfly 27*](https://docs.wildfly.org/27/Getting_Started_Guide.html)

## Instructions

Copy the http-validator.war into the "/usr/local/wildfly27/standalone/deployments" directory of your Wildfly server.

You'll have to specify Environment variable before starting the server.
Here are the mandatory environment variables to be able to deploy correctly the application :
- GZL_PROFILE_REPOSITORY_PATH
- GZL_PROFILE_CACHE_MAX_ELEMENTS
- GZL_TOKEN_API_URL
- APPLICATION_URL

Then, the specified path for GZL_PROFILE_REPOSITORY_PATH variable need to point to an existing folder, if not, you'll have to create it.
It is usually equals to /opt/http-validator/validationProfiles

Finally, start your server.

Once the application is deployed, open a browser and go to http://yourserver/http-validator/rest/metadata in order to check of the service is available.

If the deployment is successful, you should receive a 200 Ok Response.