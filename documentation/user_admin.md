---
title:  User Manual
subtitle: HTTP Validator
author: Clément LAGORCE
function: Software Engineer
date: 22/08/2023
toolversion: 0.3.0
version: 1_03
status: Reviewed
reference: KER1-MAN-IHE-HTTP-VALIDATOR_USER-1_03
customer: IHE-EUROPE
---    

# Introduction
This application has been developed with the purpose of validating any HTTP Message (Request or Response) in a full raw String format. 
- For an HTTP Request : The message is a String containing the HTTP Method, URI, Query parameters, HTTP Version and HTTP Headers.
- For an HTTP Response : The message is a String containing the HTTP Version, the Status Code, the Reason Phrase and the HTTP Headers.

Every message shall be formatted as specified in the [RFC 2616](https://www.rfc-editor.org/rfc/rfc2616) of the HTTP Protocol. This includes the double CRLF at the end of the HTTP Headers.

As we can see in [Section 2.2](https://www.rfc-editor.org/rfc/rfc2616#section-2.2), [Section 5](https://www.rfc-editor.org/rfc/rfc2616#section-5) and [Section 6](https://www.rfc-editor.org/rfc/rfc2616#section-6), the HTTP message shall be provided to the tool with the following format :
~~~text
CRLF = CR LF
CR = <US-ASCII CR, carriage return (13)>
LF = <US-ASCII LF, linefeed (10)>
SP = <US-ASCII SP, space (32)>

Request = Method SP Request-URI SP HTTP-Version CRLF          
          *(( general-header
           | request-header
           | entity-header ) CRLF)
          CRLF
          [ message-body ]
          
Response = HTTP-Version SP Status-Code SP Reason-Phrase CRLF
           *(( general-header
            | response-header
            | entity-header ) CRLF)
           CRLF
           [ message-body ]
~~~

This tool provides a way to validate automatically any HTTP Message from any standard or profile that uses RestFul Web Services.
The messages are validated against a configuration file called Validation Profile formatted in Json. 

## Purpose of the document
This document is the Test Designer user manual for the Gazelle HTTP Validator module.

## Scope of the document
The scope of this document is limited to the usage of Gazelle HTTP Validator, how to create a Validation Profile and how to perform an HTTP Message validation.

How to install, configure and maintain the tool is described in the Installation manual.

# Definitions

In this section we introduce the various concepts used in the tool and in the user manual.

## Validation Profile

A [Validation Profile](./validationProfiles/ValidationProfile.json) is a single Json file that embed all the needed rules to validate your HTTP message.
The JSON file shall contain several properties that are described and constrained in a [Json Schema](./validationProfiles/ValidationProfileSchema.json) file.
This JsonSchema file is based on the draft 2020-12 of the [Json Schema specifications](https://json-schema.org/understanding-json-schema/).
Each Validation Profile shall be valid against this Json Schema in order to be imported in this application.

### Validation Profile Properties

For a given Validation Profile the properties of the Json file shall match the following requirements :
* profileType (mandatory) : 
  * allowed value : HTTPREQUEST or HTTPRESPONSE.
  * description : The type of HTTP message that is validated by this Validation Profile.
* id (mandatory) : 
  * allowed value : Any String that is not already used by another Validation Profile.
  * description : The unique identifier of the Validation Profile.
* name (mandatory) : 
  * allowed value : Any String.
  * description : The name of the Validation Profile that may represent in 2 or 3 keywords what is the purpose of this Validation Profile.
* description (optional) :
  * allowed value : Any String.
  * description : The description of the Validation Profile to provide more information about what it validates.
* context (mandatory) :
  * allowed value : Any String.
  * description : The context in which the Validation Profile is used (Standard, IHE Profile, ...).
* referencedProfile (optional) : Not available yet.
* assertions (mandatory) : This property is Json Array that contains 1 or more Object called Assertion (see below).

## Assertion

An Assertion is a kind of rule that will be performed to validate a certain part of the HTTP message.
Each Validation Profile shall contain at least 1 Assertion in its 'assertions' property. 
Otherwise, the Validation Profile will not be imported in the application. 
Assertion object has also some mandatory and constrained properties that are validated by the Json Schema before it is imported in the application.

### Assertion Properties

For a given Assertion in the Array of Assertion of a Validation Profile, the properties shall match the following requirements :
* selector (mandatory) : This field has its own description below because it is a particular case.
* id (mandatory) :
  * allowed value : Any String that is not already used by another Assertion in the Validation Profile.
  * description : The unique identifier of the Assertion.
* description (mandatory) :
  * allowed value : Any String
  * description : The description of the Assertion. This might be a human-readable easy to understand sentence to describe what this Assertion does, as it is used as return message in the case of an error.
* requirementPriority (mandatory) :
  * allowed value : MANDATORY, RECOMMENDED, PERMITTED
  * description : MANDATORY correspond to a "shall", RECOMMENDED to a "might", PERMITTED is like authorized.
* applyIf (optional) : This field has its own description below for the same reason as selector.
* checksComposition (mandatory) :
  * allowed value : oneOf, allOf (not available yet), anyOf (not available yet), not (not available yet).
  * description : A keyword to describe the combination of checks must be valid in the case where there are more than one Check in the following 'checks' property.
* checks (mandatory) : This property is a Json Array that contains 1 or more Object called Check (see below).

### Selector and ApplyIf Properties

The String formed by the selector and applyIf properties are called Expression Language.
The selector property of the Assertion object is a particular String that will be able to resolve a subpart of the HTTP message.
By doing this, each Assertion will point to a specific part of the HTTP message and will perform its validation for this specific part.
A first constraint on this String is that it shall match with the following regex : '^(?!\$\{)(.+)(\..+){0,10}(?<!\})$'.
This regex says that the String shall be a dot separated characters with a maximum of 10 dots, doesn't start with '${' and doesn't end with '}'.
In addition, the characters between the dots shall be a keyword that is constrained by a Customised Model of HTTP message.
Each keyword is easy to understand and represents a path to access the specific part of the HTTP message that we want to validate.
Here is a representation of the model and accessible method and variables that can be used in these Expression Language :
~~~text
request
  .method
  .uri
    .path
    .queryParams('queryParamName')
      .values
  .version
  .headers('headerName')
    .values
    .parameters('parameterName')
      .values
      
response
  .version
  .statusCode
  .statusMessage
  .headers('headerName')
    .values
    .parameters('parameterName')
      .values
~~~

For example here is a sample of an HTTP Request and an HTTP Response : <br/>
~~~http request
GET /example/path?id=example_identifier&code=example_code HTTP/1.1
Host: example.net
Accept: multipart/related; type="application/dicom";transfer-syntax=1.2.840.10008.1.2.4.70;q=0.9;boundary=**
Accept: multipart/related; type="application/dicom";transfer-syntax=1.2.840.10008.1.2.4.80;q=0.8;boundary=**
Accept: multipart/related; type="application/dicom";transfer-syntax=1.2.840.10008.1.2.5;q=0.6;boundary=**

Body
~~~

Here is a non-exhaustive List of selector that are valid to select subpart of this request :
~~~text
HTTP method : "request.method" = [GET]
URI : "request.uri" = [/example/path?id=example_identifier&code=example_code]
URI Path : "request.uri.path" = [/example/path]
URI Query parameter filtered by parameter name : "request.uri.queryParams('id')" = [id=example_identifier]
URI Query parameter value filtered by parameter name : "request.uri.queryParams('id').values" = [example_identifier]
HTTP version : "request.version" = [HTTP/1.1]
HTTP Header values filtered by name : "request.headers('Accept').values" = [multipart/related; type="application/dicom";transfer-syntax=1.2.840.10008.1.2.4.70;q=0.9;boundary=**, multipart/related; type="application/dicom";transfer-syntax=1.2.840.10008.1.2.4.80;q=0.8;boundary=**, multipart/related; type="application/dicom";transfer-syntax=1.2.840.10008.1.2.5;q=0.6;boundary=**] 
The 'type' sub-parameter values for each Accept Header : "request.headers('Accept').parameters('type').values" = [application/dicom, application/dicom, application/dicom]
~~~

The applyIf property is the same type of expression as the selector, but it shall return a boolean value.
If the result is true, the Assertion will be performed as usual, else, it won't. By default, if there is no applyIf, the Assertion is of course performed.
Here is an example of a boolean Expression Language :
~~~text
Is method equals to "GET" : "request.method == 'GET'" = true
~~~

Example of an HTTP Response
~~~http request
HTTP/1.1 200 OK
Host: example.net
Content-Type: application/json

Body
~~~

We would have the same kind of expressions with an HTTP Response :
~~~text
HTTP version : "response.version" = [HTTP/1.1]
Status Code : "response.statusCode" = [200]
Status Message : "response.statusMessage" = [OK]
HTTP Header Content-type value : "response.headers('Content-Type').values" = [application/json]
~~~

## Check
A Check is a Unit Rule that shall be present at least once in the 'checks' property Json Array of each Assertion of a Validation Profile.
For the moment, we can find 4 types of Check :

Each check have a common property 'type' that shall be one of the following values : FIXEDVALUE, REGEX, CLOSEDLIST, OCCURRENCE. They also have some specific properties :
* **FixedValueCheck**
<br/>fixedValue : The String against which the selected part of the message will be compared (case-sensitive).
<br/>If the comparison is successful, this Check is valid.
~~~json
{
    "type": "FIXEDVALUE",
    "fixedValue": "example"
}
~~~
* **RegexCheck**
<br/>regex : The Regex against which the selected part of the message will be matched.
<br/>If the match is successful, this Check is valid.
~~~json
{
    "type": "REGEX",  
    "regex": "^(\\/[^\\/\\s]+)+$"
}
~~~
* **ClosedListCheck**
<br/>values : A Json enumeration ('["value1", "value2", ...]'), containing a List of String.
<br/>The selected part of the message will be compared to each of these enumerated String. If one comparison is successful, this Check is valid.
~~~json
{
    "type": "CLOSEDLIST",
    "values": ["example1", "example2"]
}
~~~
* **OccurrenceCheck**
<br/>minOccurrence : An Integer that represent the minimum number of occurrences of the selected part of the message.
<br/>maxOccurrence : An Integer that represent the maximum number of occurrences of the selected part of the message. Of course, this value shall be greater or equal to minOccurrence.
<br/>If the number of occurrence found of the selected part is equal or greater than minOccurrence, and lesser or equal to maxOccurrence, this Check is valid.
~~~json
{
    "type": "OCCURRENCE",
    "minOccurrence": 0,
    "maxOccurrence" : 1
}
~~~

# Usage

## HTTP Validator Service
This application doesn't have a front end yet, so to perform any operation with Validation Profiles, we use a Rest API.
This API is only accessible with an Authorization Token that shall have been generated with the gazelle-token-service tool, by an admin_role user on the platform where this application is deployed.
There are 4 operations allowed :
1. **Get Validation Profiles**
<br/> This operation is accessible by performing a GET HTTP Request to this kind of URL : https://example.net/http-validator/rest/validationprofiles.
It returns a Json formatted document containing the List of Validation Profiles Metadata (id and name) found on the application :
~~~json
[
    {
        "id": "example_identifier",
        "name": "example_name"
    }
]
~~~

2. **Import Validation Profile**
<br/> This operation can be performed with a POST HTTP Request to this kind of URL : https://example.net/http-validator/rest/validationprofiles, with a Json body containing a Validation Profile.
It returns one of the following type of message :
* A Status Code 400 (Bad Request) containing the list of Json Schema detected errors if any :
~~~json
[
    "$.profileType: does not have a value in the enumeration [HTTPREQUEST, HTTPRESPONSE]",
    "$.assertions[0].description: is missing but it is required",
    "There are unevaluated properties at following paths $.assertions[0].desciption",
    "$.assertions[1].checks[0].type: does not have a value in the enumeration [FIXEDVALUE, REGEX, CLOSEDLIST, OCCURRENCE]",
    "There are unevaluated properties at following paths $.assertions[1].checks[0].regex",
    "$.assertions[2].requirementPriority: does not have a value in the enumeration [MANDATORY, PERMITTED, RECOMMENDED]",
    "$.assertions[2].checksComposition: does not have a value in the enumeration [oneOf, allOf, anyOf, not]",
    "$.assertions[6].checks[0].regex: is missing but it is required",
    "There are unevaluated properties at following paths $.assertions[6].checks[0].reex"
]
~~~
* A Status Code 400 (Bad Request) with an error message if the Validation Profile is not valid for a different reason than the Json Schema Validation :
~~~text
ERROR : The ValidationProfile is not valid.
~~~
* A Status Code 400 (Bad Request) with an error message if the Validation Profile id is already in use :
~~~text
ERROR : The id example_identifier is already used.
~~~
* A Status Code 200 (OK) with a confirmation message that the Validation Profile is valid and imported :
~~~text
INFO : The ValidationProfile WADO-RS_Request_Validation_Profile is valid and imported.
~~~

3. **Import Validation Profile Batch**
<br/> This operation is available with a POST HTTP Request to this kind of URL : https://example.net/http-validator/rest/validationprofiles/batch.
A Content-Type header shall be added with "multipart/form-data" value and the body is a ZIP File containing one or more Validation Profile.
It returns the same content as the Simple Import operation.

4. **Delete Validation Profile**
<br/> This operation is a DELETE HTTP Request to this kind of URL : https://example.net/http-validator/rest/validationprofiles/delete?id=example_profile_id.
The URL shall have a query parameter 'id' equal to the Unique Identifier of Validation Profile to be deleted.
It returns :
*  A Status Code 404 (Not Found) with an error message if the provided identifier is referring to a not existing Validation Profile.
~~~text
ERROR : Unable to find ValidationProfile with id : example_profile_id.
~~~
* A Status Code 200 (OK) with a confirmation message that the Validation Profile has been successfully deleted :
~~~text
INFO : ValidationProfile (example_profile_id) successfully deleted.
~~~

## HTTP Validator Engine
This part of the application is the validation Engine that will execute rules found on the Validation Profile on the HTTP message provided to the validator.
The validation can be performed in two different ways :

### Rest API Validation
There is another Rest service to execute the validation of an HTTP message against a specific Validation Profile.
This service doesn't need an Authorization token and is available by a POST HTTP Request to this kind of URL : https://example.net/http-validator/rest/validate?profileID=example_profile_id.
It returns an XML formatted Model Based Validation Report with a detailed result of each validation rules, and some overview information.
Below is an example of the XML report  :
~~~xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<detailedResult>
    <DocumentValidXSD>
        <Result>PASSED</Result>
    </DocumentValidXSD>
    <DocumentWellFormed>
        <Result>PASSED</Result>
    </DocumentWellFormed>
    <MDAValidation>
        <Note>
            <Test>HTTP method shall be GET</Test>
            <Location>request.method</Location>
            <Description>INFO : The content is valid. </Description>
            <Identifiant>GETMethodChecking</Identifiant>
        </Note>
        <Note>
            <Test>URI path of the request shall match '^(\/[^\/\s]+)+$' regex</Test>
            <Location>request.uri.path</Location>
            <Description>INFO : The content is valid.</Description>
            <Identifiant>URIRegexChecking</Identifiant>
        </Note>
        <Note>
            <Test>HTTP Version shall be HTTP/1.1</Test>
            <Location>request.version</Location>
            <Description>INFO : The content is valid.</Description>
            <Identifiant>HTTP1VersionChecking</Identifiant>
        </Note>
        <Note>
            <Test>Each Accept headers shall have exaclty one parameter named 'type'</Test>
            <Location>request.headers('Accept').values</Location>
            <Description>INFO : The content is valid.</Description>
            <Identifiant>HeaderAcceptTypeIsUnique</Identifiant>
        </Note>
        <Note>
            <Test>Each 'type' parameters in Accept headers shall have a value application/dicom</Test>
            <Location>request.headers('Accept').parameters('type').values</Location>
            <Description>INFO : The content is valid.</Description>
            <Identifiant>HeaderAcceptTypeValueChecking</Identifiant>
        </Note>
        <Note>
            <Test>Each Accept headers shall have exaclty one parameter named 'q'</Test>
            <Location>request.headers('Accept').values</Location>
            <Description>INFO : The content is valid.</Description>
            <Identifiant>HeaderAcceptQIsUnique</Identifiant>
        </Note>
        <Note>
            <Test>Each 'type' parameters in Accept headers shall have a value application/dicom</Test>
            <Location>request.headers('Accept').parameters('q').values</Location>
            <Description>INFO : The content is valid.</Description>
            <Identifiant>HeaderAcceptQValueChecking</Identifiant>
        </Note>
        <Note>
            <Test>1 and only 1 code parameter is mandatory in the HTTP POST request.</Test>
            <Location>request.uri.queryParams('code').values</Location>
            <Description>INFO : The content is valid.</Description>
            <Identifiant>ITI71-001_code_parameter_presence</Identifiant>
        </Note>
        <Note>
            <Test>code parameter's value SHALL be a b64token; regex: [-a-zA-Z0-9._~+/]+=*.</Test>
            <Location>request.uri.queryParams('code').values</Location>
            <Description>INFO : The content is valid.</Description>
            <Identifiant>ITI71-001_code_parameter_value</Identifiant>
        </Note>
        <ValidationCounters>
            <NrOfChecks>9</NrOfChecks>
            <NrOfValidationErrors>0</NrOfValidationErrors>
            <NrOfValidationWarnings>0</NrOfValidationWarnings>
            <NrOfValidationInfos>0</NrOfValidationInfos>
            <NrOfValidationNotes>9</NrOfValidationNotes>
        </ValidationCounters>
    </MDAValidation>
    <ValidationResultsOverview>
        <ValidationDate>2023, 02 23</ValidationDate>
        <ValidationTime>07:56:54</ValidationTime>
        <ValidationServiceName>Gazelle HTTP-Validator</ValidationServiceName>
        <ValidationEngine>Example_Request_Validation_Profile</ValidationEngine>
        <ValidationTestResult>PASSED</ValidationTestResult>
        <ValidationServiceVersion>0.2.0</ValidationServiceVersion>
        <ValidationEngineVersion>1.0.0</ValidationEngineVersion>
    </ValidationResultsOverview>
</detailedResult>
~~~

### EVSClient Validation
The other way to perform HTTP message validation is by using EVSClient. A configuration shall be done in EVSClient in the same platform where HTTP-Validator is deployed.
The Validation Report is displayed in a similar manner as it is for other validators.
Below is the result of the same HTTP message against the same Validation Profile as for the Rest API :
![](./images/ValidationResultEVS.png)

# Tools needed

## HTTP Validator Service

### Sources and libraries
The sources are available on Inria’s Gitlab in the Gazelle project : _https://gitlab.inria.fr/gazelle/public/validation/http-validator.git_

Released binaries are available in the Gazelle Nexus repository at : _https://gazelle.ihe.net/nexus/#nexus-search;gav~~http-validator_

### Issue tracker
Issues and features requests shall be entered through the issue bug tracking system of the Gazelle platform, at _https://gazelle.ihe.net/jira/browse/HTTP_VAL_.

## EVSClient

### Sources and libraries
The sources are available on Inria’s Gitlab in the Gazelle project : _https://gitlab.inria.fr/gazelle/applications/test-execution/validator/EVSClient_.

Released binaries are available in the Gazelle Nexus repository at : _https://gazelle.ihe.net/nexus/index.html#nexus-search;gav~~EVSClient_

### Issue tracker
For an issue specific to the gazelle-x-validation module, please use the issue tracker at _https://gazelle.ihe.net/jira/browse/HTTP_VAL_. For an issue specific to the EVS Client, you can use _https://gazelle.ihe.net/jira/browse/EVSCLT_.

## Gazelle Token Service

### Sources and libraries
The sources are available on Inria’s Gitlab in the Gazelle project : _https://gitlab.inria.fr/gazelle/applications/core/gazelle-token_.

Released binaries are available in the Gazelle Nexus repository at : _https://gazelle.ihe.net/nexus/index.html#nexus-search;quick~gazelle-token_.