package net.ihe.gazelle.httpvalidatorengine.domain.httpmodel;

import java.util.ArrayList;
import java.util.List;

public class ParameterList {

    private List<Parameter> parameters = new ArrayList<>();

    public ParameterList() {
    }

    public ParameterList(List<Parameter> parameters) {
        setParameters(parameters);
    }

    public List<Parameter> getParameters() {
        return new ArrayList<>(this.parameters);
    }

    public ParameterList setParameters(List<Parameter> parameters) {
        this.parameters = new ArrayList<>();
        if (parameters != null) {
            this.parameters.addAll(parameters);
        }
        return this;
    }

    public void addAll(List<Parameter> parameters) {
        this.parameters.addAll(parameters);
    }

    @Override
    public String toString() {
        return this.parameters.toString();
    }

    public List<String> getValues() {
        return this.parameters.stream()
                .map(Parameter::getValue)
                .toList();
    }
}
