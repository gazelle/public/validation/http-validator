package net.ihe.gazelle.httpvalidatorengine.interlay.checkers.occurrence;

import net.ihe.gazelle.httpvalidatorengine.application.engine.Checker;
import net.ihe.gazelle.httpvalidatorengine.domain.engine.CheckerReports;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.EngineMessage;

import java.util.List;

public class OccurrenceChecker implements Checker {

    private OccurrenceCheck occurrenceCheck;

    @Override
    public void validate(List<String> contentList, Check check, CheckerReports checkerReports) {
        occurrenceCheck = (OccurrenceCheck) check;
        String description = checkerReports.getAssertion().getDescription();
        if (!isValid(contentList)) {
            String error = EngineMessage.OCCURRENCE_VALUE_ERROR.getFormattedValue(description, contentList, occurrenceCheck.getMinOccurrence(), occurrenceCheck.getMaxOccurrence(), contentList.size());
            checkerReports.addError(error);
        } else {
            String info = EngineMessage.CHECKER_INFO.getFormattedValue();
            checkerReports.addInfo(info);
        }
    }

    private boolean isValid(List<String> contentList) {
        return contentList.size() >= occurrenceCheck.getMinOccurrence() &&
               contentList.size() <= occurrenceCheck.getMaxOccurrence();
    }
}
