package net.ihe.gazelle.httpvalidatorengine.domain.validationprofile;

import net.ihe.gazelle.validation.api.domain.report.structure.RequirementPriority;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Assertion implements Serializable {

    @Serial
    private static final long serialVersionUID = 5701180328105120001L;
    private static final String SELECTOR_PATTERN = "^(.+)(\\..+){0,10}$";
    private String selector;
    private String id;
    private String description;
    private RequirementPriority requirementPriority;
    private String applyIf;
    private String checksComposition;
    private List<Check> checks;

    public String getSelector() {
        return selector;
    }

    public Assertion setSelector(String selector) {
        this.selector = selector;
        return this;
    }

    public String getId() {
        return id;
    }

    public Assertion setId(String id) {
        this.id = id;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Assertion setDescription(String description) {
        this.description = description;
        return this;
    }

    public RequirementPriority getRequirementPriority() {
        return requirementPriority;
    }

    public Assertion setRequirementPriority(RequirementPriority requirementPriority) {
        this.requirementPriority = requirementPriority;
        return this;
    }

    public String getApplyIf() {
        return applyIf;
    }

    public Assertion setApplyIf(String applyIf) {
        this.applyIf = applyIf;
        return this;
    }

    public String getChecksComposition() {
        return checksComposition;
    }

    public Assertion setChecksComposition(String checksComposition) {
        this.checksComposition = checksComposition;
        return this;
    }

    public List<Check> getChecks() {
        return new ArrayList<>(checks);
    }

    public Assertion setChecks(List<Check> checks) {
        this.checks = new ArrayList<>(checks);
        return this;
    }

    public boolean isValid() {
        return this.selector != null && this.selector.matches(SELECTOR_PATTERN) &&
                this.id != null && !this.id.isEmpty() && !this.id.isBlank() &&
                this.requirementPriority != null &&
                this.checks != null && !this.checks.isEmpty() && isCheckListValid();
    }

    private boolean isCheckListValid() {
        for (Check check : checks) {
            if (!check.isValid()) {
                return false;
            }
        }
        return true;
    }
}
