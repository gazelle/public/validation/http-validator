package net.ihe.gazelle.httpvalidatorengine.interlay.checkers.fixedvalue;

import net.ihe.gazelle.httpvalidatorengine.application.engine.Checker;
import net.ihe.gazelle.httpvalidatorengine.domain.engine.CheckerReports;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.EngineMessage;

import java.util.List;

public class FixedValueChecker implements Checker {

    private FixedValueCheck fixedValueCheck;

    @Override
    public void validate(List<String> contentList, Check check, CheckerReports checkerReports) {
        fixedValueCheck = (FixedValueCheck) check;
        Assertion assertion = checkerReports.getAssertion();
        if (contentList.isEmpty()) {
            String emptySelector = EngineMessage.SELECTOR_NOT_EXIST.getFormattedValue(assertion.getSelector());
            checkerReports.addError(emptySelector);
        } else {
            if (!isValid(contentList)) {
                String error = EngineMessage.FIXED_VALUE_ERROR.getFormattedValue(assertion.getDescription(), contentList.toString(), fixedValueCheck.getFixedValue());
                checkerReports.addError(error);
            } else {
                String info = EngineMessage.CHECKER_INFO.getFormattedValue();
                checkerReports.addInfo(info);
            }
        }
    }

    private boolean isValid(List<String> contentList) {
        return contentList.stream().allMatch(content -> fixedValueCheck.getFixedValue().equals(content));
    }
}
