package net.ihe.gazelle.httpvalidatorengine.interlay.utils;

import java.text.MessageFormat;
import java.util.Properties;

public enum EngineMessage {

    SELECTOR_MAL_FORMED,
    SELECTOR_NOT_EXIST,
    ONE_OF_ERROR,
    CHECKER_INFO,
    FIXED_VALUE_ERROR,
    REGEX_VALUE_ERROR,
    CLOSED_LIST_ERROR,
    OCCURRENCE_VALUE_ERROR;

    private static final String MESSAGE_FILE_NAME = "engineMessages.properties";
    private static final Properties PROPERTIES;

    static {
        PROPERTIES = FileUtils.loadPropertiesFromResource(MESSAGE_FILE_NAME);
    }

    public String getFormattedValue(Object ... arguments) {
        return MessageFormat.format(PROPERTIES.getProperty(this.name()), arguments);
    }
}
