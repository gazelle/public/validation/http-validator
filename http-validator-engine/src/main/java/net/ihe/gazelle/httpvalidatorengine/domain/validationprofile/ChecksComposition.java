package net.ihe.gazelle.httpvalidatorengine.domain.validationprofile;

public enum ChecksComposition {
    ONE_OF("oneOf"),
    ANY_OF("anyOf"),
    ALL_OF("allOf"),
    NOT("not");

    private final String value;

    ChecksComposition(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public static ChecksComposition findByValue(String value) {
        for (ChecksComposition composition : ChecksComposition.values()) {
            if (value.equals(composition.getValue())) {
                return composition;
            }
        }
        throw new IllegalStateException("Unknown ChecksComposition value");
    }

}
