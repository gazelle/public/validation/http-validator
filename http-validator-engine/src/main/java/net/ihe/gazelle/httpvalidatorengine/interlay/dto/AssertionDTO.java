package net.ihe.gazelle.httpvalidatorengine.interlay.dto;

import jakarta.json.bind.annotation.*;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.AbstractCheckDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.factory.CheckDTOFactory;
import net.ihe.gazelle.validation.api.domain.report.structure.RequirementPriority;

import java.util.List;

@JsonbPropertyOrder({"selector", "id", "description", "requirementPriority", "applyIf", "checksComposition", "checks"})
public class AssertionDTO {

    private Assertion assertion;
    private CheckDTOFactory checkDTOFactory;

    public AssertionDTO() {
        this.assertion = new Assertion();
    }

    @JsonbProperty(value = "selector")
    public String getSelector() {
        return this.assertion.getSelector();
    }

    public AssertionDTO setSelector(String selector) {
        this.assertion.setSelector(selector);
        return this;
    }

    @JsonbProperty(value = "id")
    public String getId() {
        return this.assertion.getId();
    }

    public AssertionDTO setId(String id) {
        this.assertion.setId(id);
        return this;
    }

    @JsonbProperty(value = "description")
    public String getDescription() {
        return this.assertion.getDescription();
    }

    public AssertionDTO setDescription(String description) {
        this.assertion.setDescription(description);
        return this;
    }

    @JsonbProperty(value = "requirementPriority")
    public RequirementPriority getRequirementPriority() {
        return this.assertion.getRequirementPriority();
    }


    public AssertionDTO setRequirementPriority(RequirementPriority requirementPriority) {
        this.assertion.setRequirementPriority(requirementPriority);
        return this;
    }

    @JsonbProperty(value = "applyIf")
    public String getApplyIf() {
        return this.assertion.getApplyIf();
    }

    public AssertionDTO setApplyIf(String applyIf) {
        this.assertion.setApplyIf(applyIf);
        return this;
    }

    @JsonbProperty(value = "checksComposition")
    public String getChecksComposition() {
        return this.assertion.getChecksComposition();
    }

    public AssertionDTO setChecksComposition(String checksComposition) {
        this.assertion.setChecksComposition(checksComposition);
        return this;
    }

    @JsonbProperty(value = "checks")
    public List<AbstractCheckDTO> getChecks() {
        return this.assertion.getChecks().stream().map(
                checkDTOFactory::create
        ).toList();
    }

    public AssertionDTO setChecks(List<AbstractCheckDTO> checks) {
        this.assertion.setChecks(checks.stream().map(
                AbstractCheckDTO::getCheck
        ).toList());
        return this;
    }

    @JsonbTransient
    public Assertion getDomainObject() {
        return this.assertion;
    }

    public AssertionDTO setDomainObject(Assertion domain) {
        this.assertion = domain;
        return this;
    }

    public AssertionDTO setCheckDTOFactory(CheckDTOFactory checkDTOFactory) {
        this.checkDTOFactory = checkDTOFactory;
        return this;
    }

    @JsonbTransient
    public boolean isValid() {
        return assertion.isValid();
    }
}
