package net.ihe.gazelle.httpvalidatorengine.domain.engine;

import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;

import java.util.ArrayList;
import java.util.List;

public class CheckerReports {

    private final List<String> contentList;
    private final Assertion assertion;
    private final List<String> errors = new ArrayList<>();
    private final List<String> infos = new ArrayList<>();

    public CheckerReports(List<String> contentList, Assertion assertion) {
        this.contentList = new ArrayList<>(contentList);
        this.assertion = assertion;
    }

    public List<String> getContentList() {
        return new ArrayList<>(contentList);
    }

    public Assertion getAssertion() {
        return assertion;
    }

    public List<String> getErrors() {
        return new ArrayList<>(errors);
    }

    public List<String> getInfos() {
        return new ArrayList<>(infos);
    }

    public void addInfo(String info) {
        this.infos.add(info);
    }

    public void addError(String error) {
        this.errors.add(error);
    }

    public String errorsToString() {
        return String.join("\n", errors);
    }

    public String infosToString() {
        return String.join("\n", infos);
    }

    @Override
    public String toString() {
        return infosToString() + "\n" + errorsToString();
    }
}
