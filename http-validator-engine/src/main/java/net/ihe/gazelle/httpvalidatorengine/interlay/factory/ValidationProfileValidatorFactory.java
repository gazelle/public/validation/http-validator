package net.ihe.gazelle.httpvalidatorengine.interlay.factory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import net.ihe.gazelle.httpvalidatorengine.application.profilevalidator.ValidationProfileValidator;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileAdapter;
import net.ihe.gazelle.httpvalidatorengine.interlay.profilevalidator.ValidationProfileValidatorImpl;

@ApplicationScoped
public class ValidationProfileValidatorFactory {

    @Inject
    ValidationProfileAdapter adapter;

    @Produces
    public ValidationProfileValidator createValidationProfileValidator() {
        return new ValidationProfileValidatorImpl(adapter);
    }
}
