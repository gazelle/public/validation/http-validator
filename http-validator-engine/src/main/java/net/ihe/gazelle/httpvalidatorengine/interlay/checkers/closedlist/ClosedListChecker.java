package net.ihe.gazelle.httpvalidatorengine.interlay.checkers.closedlist;

import net.ihe.gazelle.httpvalidatorengine.application.engine.Checker;
import net.ihe.gazelle.httpvalidatorengine.domain.engine.CheckerReports;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.EngineMessage;

import java.util.List;

public class ClosedListChecker implements Checker {

    private ClosedListCheck closedListCheck;
    private String currentElement;

    @Override
    public void validate(List<String> contentList, Check check, CheckerReports assertionReport) {
        closedListCheck = (ClosedListCheck) check;
        Assertion assertion = assertionReport.getAssertion();
        if (contentList.isEmpty()) {
            String emptySelector = EngineMessage.SELECTOR_NOT_EXIST.getFormattedValue(assertion.getSelector());
            assertionReport.addError(emptySelector);
        } else {
            if (!isValid(contentList)) {
                String error = EngineMessage.CLOSED_LIST_ERROR.getFormattedValue(assertion.getDescription(), this.currentElement, closedListCheck.getValues().toString());
                assertionReport.addError(error);
            } else {
                String info = EngineMessage.CHECKER_INFO.getFormattedValue();
                assertionReport.addInfo(info);
            }
        }
    }

    private boolean isValid(List<String> contentList) {
        for (String content : contentList) {
            this.currentElement = content;
            boolean isMatchingOneValue = false;
            for (String value : closedListCheck.getValues()) {
                if (content.equals(value)) {
                    isMatchingOneValue = true;
                    break;
                }
            }
            if (!isMatchingOneValue) {
                return false;
            }
        }
        return true;
    }
}
