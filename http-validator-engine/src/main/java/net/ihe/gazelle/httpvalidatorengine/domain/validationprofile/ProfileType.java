package net.ihe.gazelle.httpvalidatorengine.domain.validationprofile;

public enum ProfileType {
    HTTPREQUEST, HTTPRESPONSE
}
