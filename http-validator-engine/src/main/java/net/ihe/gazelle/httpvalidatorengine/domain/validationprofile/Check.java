package net.ihe.gazelle.httpvalidatorengine.domain.validationprofile;

import java.io.Serializable;

public interface Check extends Serializable {
    boolean isValid();
}
