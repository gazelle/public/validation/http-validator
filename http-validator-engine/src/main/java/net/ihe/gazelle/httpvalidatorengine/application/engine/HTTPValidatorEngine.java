package net.ihe.gazelle.httpvalidatorengine.application.engine;

import net.ihe.gazelle.httpvalidatorengine.domain.engine.CheckerReports;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ChecksComposition;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.domain.httpmodel.Message;
import net.ihe.gazelle.httpvalidatorengine.interlay.factory.CheckerFactory;
import net.ihe.gazelle.httpvalidatorengine.interlay.validationreport.HttpAssertionResultHandler;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationSubReport;

import java.util.List;

public class HTTPValidatorEngine implements Engine {

    private final EngineFactory engineFactory;
    private final CheckerFactory checkerFactory;
    private HttpAssertionResultHandler handler;

    public HTTPValidatorEngine(EngineFactory engineFactory, CheckerFactory checkerFactory) {
        this.engineFactory = engineFactory;
        this.checkerFactory = checkerFactory;
    }

    @Override
    public ValidationSubReport validate(String httpMessage, ValidationProfile validationProfile) {
        ValidationSubReport validationSubReport = new ValidationSubReport().setName(validationProfile.getName());
        this.handler = new HttpAssertionResultHandler(validationSubReport);
        Message message = engineFactory.createParser().parse(httpMessage, validationProfile.getProfileType());
        for (Assertion assertion : validationProfile.getAssertions()) {
            validateAssertion(assertion, message);
        }
        return handler.getValidationSubReport();
    }

    private void validateAssertion(Assertion assertion, Message message) {
        ELEngine elEngine = engineFactory.createELEngine();
        if (elEngine.applyIf(message, assertion, handler)) {
            List<String> contentList = elEngine.select(message, assertion, handler);
            if (elEngine.isEvaluated()) {
                validateCheckers(assertion, contentList);
            }
        }
    }

    private void validateCheckers(Assertion assertion, List<String> contentList) {
        CheckerReports assertionReport = new CheckerReports(contentList, assertion);
        for (Check check : assertion.getChecks()) {
            Checker checker = checkerFactory.create(check);
            checker.validate(contentList, check, assertionReport);
        }
        engineFactory.createComposition().validate(ChecksComposition.findByValue(assertion.getChecksComposition()), assertionReport, handler);
    }
}
