package net.ihe.gazelle.httpvalidatorengine.interlay.parser;

import net.ihe.gazelle.httpvalidatorengine.application.exceptions.HTTPParsingException;
import net.ihe.gazelle.httpvalidatorengine.domain.httpmodel.*;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ProfileType;
import rawhttp.core.*;
import rawhttp.core.errors.InvalidHttpRequest;
import rawhttp.core.errors.InvalidHttpResponse;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class HttpMessageParser {

    private ProfileType profileType;

    public Message parse(String httpMessage, ProfileType profileType) {
        this.profileType = profileType;
        RawHttpOptions.Builder builder = RawHttpOptions.newBuilder()
                .doNotInsertHttpVersionIfMissing();
        builder.withHttpHeadersOptions().withMaxHeaderValueLength(15000);
        RawHttpOptions parseOptions = builder.build();
        RawHttp rawHttp = new RawHttp(parseOptions);
        return parseAnyHTTPMessageToModel(httpMessage, rawHttp);
    }

    private Message parseAnyHTTPMessageToModel(String httpMessage, RawHttp rawHttp) {
        switch (profileType) {
            case HTTPREQUEST -> {
                return parseRequest(httpMessage, rawHttp);
            }
            case HTTPRESPONSE -> {
                return parseResponse(httpMessage, rawHttp);
            }
            default -> throw new HTTPParsingException("Invalid HTTP message: No parsing method corresponding to ValidationProfile type");
        }
    }

    private Request parseRequest(String httpMessage, RawHttp rawHttp) {
        try {
            RawHttpRequest request = rawHttp.parseRequest(httpMessage);
            RequestLine requestLine = request.getStartLine();
            RawHttpHeaders headers = request.getHeaders();
            return new Request()
                    .setMethod(requestLine.getMethod())
                    .setUri(parseURI(requestLine.getUri()))
                    .setVersion(requestLine.getHttpVersion().toString())
                    .setMessageHeaders(parseHeader(headers));
        } catch (InvalidHttpRequest e) {
            throw new HTTPParsingException(e.getMessage() + " at line " + e.getLineNumber(), e.getCause());
        }
    }

    private Response parseResponse(String httpMessage, RawHttp rawHttp) {
        try {
            RawHttpResponse<Void> response = rawHttp.parseResponse(httpMessage);
            StatusLine statusLine = response.getStartLine();
            RawHttpHeaders headers = response.getHeaders();
            return new Response()
                    .setStatusCode(statusLine.getStatusCode())
                    .setStatusMessage(statusLine.getReason())
                    .setVersion(statusLine.getHttpVersion().toString())
                    .setMessageHeaders(parseHeader(headers));
        } catch (InvalidHttpResponse e) {
            throw new HTTPParsingException(e.getMessage() + " at line " + e.getLineNumber(), e.getCause());
        }
    }

    public List<HeaderModel> parseHeader(RawHttpHeaders headers) {
        List<HeaderModel> headerModels = new ArrayList<>();
        Set<String> uniqueNames = headers.getUniqueHeaderNames();
        for (String uniquesName : uniqueNames) {
            BasicHeader header = headers.get(uniquesName);
            HeaderModel headerModel = parseHeaderFromRawHttpHeader(header);
            headerModels.add(headerModel);
        }
        return headerModels;
    }

    private HeaderModel parseHeaderFromRawHttpHeader(BasicHeader header) {
        List<HeaderValue> headerValues = new ArrayList<>();
        for (HeaderElement headerElement : header.getElements()) {
            HeaderValue headerValue = new HeaderValue();
            if (headerElement.getValue() != null && !headerElement.getValue().isEmpty()) {
                List<Parameter> parameters = List.of(new Parameter(headerElement.getName(), headerElement.getValue()));
                headerValue.addAllParameters(new ParameterList(parameters));
            } else {
                headerValue.setValue(headerElement.getName());
            }
            if (headerElement.getParameters().length > 0) {
                headerValue.addAllParameters(parseMultiPart(headerElement.getParameters()));
            }
            headerValues.add(headerValue);
        }
        return new HeaderModel(header.getName(), headerValues);
    }

    private ParameterList parseMultiPart(NameValuePair[] parameters) {
        List<Parameter> headerValueParameterModels = new ArrayList<>();
        for (NameValuePair nameValuePair : parameters) {
            headerValueParameterModels.add(new Parameter(nameValuePair.getName(), nameValuePair.getValue()));
        }
        return new ParameterList(headerValueParameterModels);
    }

    public URIModel parseURI(URI uri) {
        return new URIModel()
                .setScheme(uri.getScheme())
                .setAuthority(uri.getAuthority())
                .setPath(uri.getPath())
                .setQueryParams(parseQueryParams(uri.getQuery()));
    }

    private static ParameterList parseQueryParams(String query) {
        List<Parameter> queryParamModels = new ArrayList<>();
        if (query != null && query.length() > 1) {
            String[] queryParts = query.split("&");
            for (String queryPart : queryParts) {
                String[] keyValue = queryPart.split("=");
                if (keyValue.length > 1) {
                    queryParamModels.add(new Parameter(keyValue[0], keyValue[1]));
                } else {
                    queryParamModels.add(new Parameter(keyValue[0], ""));
                }
            }
        }
        return new ParameterList(queryParamModels);
    }

}
