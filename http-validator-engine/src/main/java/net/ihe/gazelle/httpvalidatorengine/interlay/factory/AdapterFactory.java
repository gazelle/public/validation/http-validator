package net.ihe.gazelle.httpvalidatorengine.interlay.factory;

import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileAdapter;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileAdapterImpl;

public class AdapterFactory {

    @Inject
    private DTOFactory dtoFactory;

    @Produces
    public ValidationProfileAdapter createAdapter() {
        return new ValidationProfileAdapterImpl(dtoFactory);
    }
}
