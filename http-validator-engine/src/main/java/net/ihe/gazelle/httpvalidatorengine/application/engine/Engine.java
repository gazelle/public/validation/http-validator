package net.ihe.gazelle.httpvalidatorengine.application.engine;

import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationSubReport;

public interface Engine {
    ValidationSubReport validate(String content, ValidationProfile validationProfile);
}
