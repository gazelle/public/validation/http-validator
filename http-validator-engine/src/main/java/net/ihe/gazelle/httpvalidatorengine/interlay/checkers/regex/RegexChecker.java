package net.ihe.gazelle.httpvalidatorengine.interlay.checkers.regex;

import net.ihe.gazelle.httpvalidatorengine.application.engine.Checker;
import net.ihe.gazelle.httpvalidatorengine.domain.engine.CheckerReports;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.EngineMessage;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexChecker implements Checker {

    private RegexCheck regexCheck;

    @Override
    public void validate(List<String> contentList, Check check, CheckerReports assertionReport) {
        regexCheck = (RegexCheck) check;
        Assertion assertion = assertionReport.getAssertion();
        if (contentList.isEmpty()) {
            String emptySelector = EngineMessage.SELECTOR_NOT_EXIST.getFormattedValue(assertion.getSelector());
            assertionReport.addError(emptySelector);
        } else {
            if (!isValid(contentList)) {
                String error = EngineMessage.REGEX_VALUE_ERROR.getFormattedValue(assertion.getDescription(), contentList.toString(), regexCheck.getRegex());
                assertionReport.addError(error);
            } else {
                String info = EngineMessage.CHECKER_INFO.getFormattedValue();
                assertionReport.addInfo(info);
            }
        }
    }

    private boolean isValid(List<String> contentList) {
        Pattern pattern = Pattern.compile(regexCheck.getRegex());
        for (String content : contentList) {
            Matcher matcher = pattern.matcher(content);
            if (!(matcher.find() || matcher.matches())) {
                return false;
            }
        }
        return true;
    }
}
