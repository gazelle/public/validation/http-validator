package net.ihe.gazelle.httpvalidatorengine.interlay.dto;

import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;

public interface ValidationProfileAdapter {
    String adaptToString(ValidationProfile validationProfile);

    ValidationProfile adaptFromString(String jsonString);
}
