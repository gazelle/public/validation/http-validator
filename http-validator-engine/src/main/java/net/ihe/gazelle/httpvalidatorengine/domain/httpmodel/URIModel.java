package net.ihe.gazelle.httpvalidatorengine.domain.httpmodel;

import java.util.List;
import java.util.Objects;

public class URIModel {

    private String scheme;
    private String authority;
    private String path;
    private ParameterList queryParams;

    public URIModel() {
        this(null, null);
    }

    public URIModel(String path) {
        this(path, null);
    }

    public URIModel(String path, ParameterList queryParams) {
        this.path = path;
        this.queryParams = Objects.requireNonNullElseGet(queryParams, ParameterList::new);
    }

    public String getScheme() {
        return scheme;
    }

    public URIModel setScheme(String scheme) {
        this.scheme = scheme;
        return this;
    }

    public String getAuthority() {
        return authority;
    }

    public URIModel setAuthority(String authority) {
        this.authority = authority;
        return this;
    }

    public String getPath() {
        return path;
    }

    public URIModel setPath(String path) {
        this.path = path;
        return this;
    }

    public ParameterList getQueryParams() {
        return this.queryParams;
    }

    public URIModel setQueryParams(ParameterList queryParams) {
        this.queryParams = queryParams;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(path);
        List<Parameter> parameterList = queryParams.getParameters();
        if (!parameterList.isEmpty()) {
            stringBuilder.append("?");
            for (int i = 0; i < parameterList.size(); i++) {
                stringBuilder.append(parameterList.get(i).toString());
                if (i != parameterList.size() - 1) {
                    stringBuilder.append("&");
                }
            }
        }
        return stringBuilder.toString();
    }

    public ParameterList queryParams(String queryParamName) {
        return new ParameterList(queryParams.getParameters().stream().filter(
                parameter -> parameter.getName().equals(queryParamName)
        ).toList());
    }
}
