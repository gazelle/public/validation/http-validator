package net.ihe.gazelle.httpvalidatorengine.application.engine;

import jakarta.el.*;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.EngineException;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.SelectorException;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.httpvalidatorengine.interlay.validationreport.HttpAssertionResultHandler;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.EngineMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ELEngine {

    private final Logger logger = LoggerFactory.getLogger(ELEngine.class);
    private boolean isEvaluated;

    public boolean isEvaluated() {
        return isEvaluated;
    }

    public List<String> select(Object contextBean, Assertion assertion, HttpAssertionResultHandler handler) {
        isEvaluated = true;
        String expression = "${" + assertion.getSelector() + "}";
        try {
            ExpressionFactory factory = ExpressionFactory.newInstance();
            StandardELContext context = new ELManager().getELContext();
            ELResolver resolver = context.getELResolver();

            resolver.setValue(context,null, contextBean.getClass().getSimpleName().toLowerCase(), contextBean);

            return findExpressionType(factory.createValueExpression(context, expression, Object.class).getValue(context));
        } catch (ELException e) {
            this.isEvaluated = false;
            filterException(e, expression, assertion, handler);
            return Collections.emptyList();
        }
    }

    public boolean applyIf(Object contextBean, Assertion assertion, HttpAssertionResultHandler handler) {
        isEvaluated = true;
        String expression = assertion.getApplyIf();
        if (expression == null) {
            return true;
        } else {
            expression = "${" + assertion.getApplyIf() + "}";
        }
        try {
            ExpressionFactory factory = ExpressionFactory.newInstance();
            StandardELContext context = new ELManager().getELContext();
            ELResolver resolver = context.getELResolver();

            resolver.setValue(context,null, contextBean.getClass().getSimpleName().toLowerCase(), contextBean);

            return factory.createValueExpression(context, expression, Boolean.class).getValue(context);
        } catch (ELException e) {
            this.isEvaluated = false;
            filterException(e, expression, assertion, handler);
            return false;
        }
    }

    private void filterException(ELException e, String expression, Assertion assertion, HttpAssertionResultHandler handler) {
        if (e.getCause() != null && e.getCause().getClass().equals(SelectorException.class)) {
            handler.error(e.getCause().getMessage(), assertion);
        } else {
            logger.error(e.getMessage());
            throw new EngineException(EngineMessage.SELECTOR_MAL_FORMED.getFormattedValue(expression, assertion.getId()), e.getCause());
        }
    }

    private List<String> findExpressionType(Object contentSelected) {
        List<String> contentList = new ArrayList<>();
        if (contentSelected instanceof String contentString) {
            contentList.add(contentString);
        } else if (contentSelected instanceof List<?>) {
            contentList.addAll(getListOfStringFromObject(contentSelected));
        } else {
            contentList.add(contentSelected.toString());
        }
        return contentList;
    }

    private List<String> getListOfStringFromObject(Object objectList) {
        List<String> stringList = new ArrayList<>();
        if (objectList instanceof List<?>) {
            for (Object object : (List<?>) objectList) {
                if (object instanceof String objectString) {
                    stringList.add(objectString);
                } else {
                    stringList.add(object.toString());
                }
            }
        }
        return stringList;
    }
}
