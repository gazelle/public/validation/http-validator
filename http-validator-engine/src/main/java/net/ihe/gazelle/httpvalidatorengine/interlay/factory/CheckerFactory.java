package net.ihe.gazelle.httpvalidatorengine.interlay.factory;

import jakarta.enterprise.context.ApplicationScoped;
import net.ihe.gazelle.httpvalidatorengine.application.engine.Checker;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;

import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class CheckerFactory extends AbstractCheckFactory<Checker> {

    private static final Map<Class<?>, Class<? extends Checker>> cache = new HashMap<>();

    public CheckerFactory() {
        initCache();
    }

    public Checker create(Check check) {
        return getInstance(cache, check);
    }

    private void initCache() {
        cache.putAll(init(Checker.class));
    }

    @Override
    protected void findCheckType(Map<Class<?>, Class<? extends Checker>> cache, Class<? extends Checker> subClass) {
        Class<?> domainModelClass = subClass.getDeclaredFields()[0].getType();
        cache.put(domainModelClass, subClass);
    }
}
