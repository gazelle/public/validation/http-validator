package net.ihe.gazelle.httpvalidatorengine.interlay.profilevalidator;

import java.util.ArrayList;
import java.util.List;

public enum ELExpressionBlackList {
    CLASS("class"),
    GET_CLASS_LOADER("getClassLoader"),
    LOAD_CLASS("loadClass"),
    JAVA_LANG_RUNTIME("java.lang.Runtime"),
    GET_METHOD("getMethod"),
    GET_RUNTIME("getRuntime"),
    INVOKE("invoke"),
    EXEC("exec");

    private final String value;

    ELExpressionBlackList(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static List<String> getValueList() {
        List<String> values = new ArrayList<>();
        ELExpressionBlackList[] list = ELExpressionBlackList.values();
        for (ELExpressionBlackList item : list) {
            values.add(item.getValue());
        }
        return values;
    }
}
