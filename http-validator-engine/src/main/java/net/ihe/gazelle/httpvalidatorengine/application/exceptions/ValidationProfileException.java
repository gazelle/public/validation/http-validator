package net.ihe.gazelle.httpvalidatorengine.application.exceptions;

public class ValidationProfileException extends RuntimeException {

    public ValidationProfileException(String message) {
        super(message);
    }

    public ValidationProfileException(String message, Throwable cause) {
        super(message, cause);
    }
}
