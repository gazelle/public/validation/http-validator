package net.ihe.gazelle.httpvalidatorengine.application.exceptions;

public class EngineException extends RuntimeException {

    public EngineException(String message) {
        super(message);
    }

    public EngineException(String message, Throwable cause) {
        super(message, cause);
    }
}
