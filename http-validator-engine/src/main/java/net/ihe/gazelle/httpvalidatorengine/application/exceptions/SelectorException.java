package net.ihe.gazelle.httpvalidatorengine.application.exceptions;

public class SelectorException extends RuntimeException {

    public SelectorException(String message) {super(message);}
}
