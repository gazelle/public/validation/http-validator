package net.ihe.gazelle.httpvalidatorengine.domain.httpmodel;

import java.util.List;

public class Response extends Message {

    private int statusCode;
    private String statusMessage;

    public int getStatusCode() {
        return statusCode;
    }

    public Response setStatusCode(int statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public Response setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    @Override
    public Response setVersion(String version) {
        return (Response) super.setVersion(version);
    }

    @Override
    public Response setMessageHeaders(List<HeaderModel> headers) {
        return (Response) super.setMessageHeaders(headers);
    }

    @Override
    public String toString() {
        return super.getVersion() + " " + this.statusCode + " " + this.statusMessage + "\n\n" + super.toString();
    }
}
