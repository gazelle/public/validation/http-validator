package net.ihe.gazelle.httpvalidatorengine.interlay.factory;

import jakarta.enterprise.context.ApplicationScoped;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.AbstractCheckDTO;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class CheckDTOFactory extends AbstractCheckFactory<AbstractCheckDTO> {

    private static final Map<Class<?>, Class<? extends AbstractCheckDTO>> cache = new HashMap<>();

    public CheckDTOFactory() {
        initCache();
    }

    public AbstractCheckDTO create(Check check) {
        return getInstance(cache, check).setCheck(check);
    }

    private void initCache() {
        cache.putAll(init(AbstractCheckDTO.class));
    }

    @Override
    protected void findCheckType(Map<Class<?>, Class<? extends AbstractCheckDTO>> cache, Class<? extends AbstractCheckDTO> subClass) {
        Optional<Method> optional = Arrays.stream(subClass.getDeclaredMethods()).filter(
                m -> m.getName().startsWith("getDomain") && !m.getReturnType().equals(Object.class)
        ).findFirst();
        if (optional.isPresent()) {
            Class<?> domainModelClass = optional.get().getReturnType();
            cache.put(domainModelClass, subClass);
        }
    }
}
