package net.ihe.gazelle.httpvalidatorengine.interlay.validationreport;

import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.validation.api.domain.report.structure.AssertionReport;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationTestResult;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationSubReport;

public class HttpAssertionResultHandler {

    private final ValidationSubReport validationSubReport;

    public HttpAssertionResultHandler(ValidationSubReport validationSubReport) {
        this.validationSubReport = validationSubReport;
    }

    public ValidationSubReport getValidationSubReport() {
        return this.validationSubReport;
    }

    public void error(String infoMessage, Assertion assertion) {
        AssertionReport assertionReport = new AssertionReport()
                .setResult(ValidationTestResult.FAILED)
                .setPriority(assertion.getRequirementPriority())
                .setAssertionID(assertion.getId())
                .setDescription(infoMessage);
        this.validationSubReport.addAssertionReport(assertionReport);
    }

    public void info(String infoMessage, Assertion assertion) {
        AssertionReport assertionReport = new AssertionReport()
                .setResult(ValidationTestResult.PASSED)
                .setPriority(assertion.getRequirementPriority())
                .setAssertionID(assertion.getId())
                .setDescription(infoMessage);
        this.validationSubReport.addAssertionReport(assertionReport);
    }
}
