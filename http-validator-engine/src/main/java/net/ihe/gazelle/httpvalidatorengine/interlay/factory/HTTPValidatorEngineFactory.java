package net.ihe.gazelle.httpvalidatorengine.interlay.factory;

import jakarta.enterprise.context.RequestScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import net.ihe.gazelle.httpvalidatorengine.application.engine.Engine;
import net.ihe.gazelle.httpvalidatorengine.application.engine.EngineFactory;
import net.ihe.gazelle.httpvalidatorengine.application.engine.HTTPValidatorEngine;

@RequestScoped
public class HTTPValidatorEngineFactory {

    @Inject
    EngineFactory engineFactory;
    @Inject
    CheckerFactory checkerFactory;

    @Produces
    public Engine createEngine() {
        return new HTTPValidatorEngine(engineFactory, checkerFactory);
    }
}
