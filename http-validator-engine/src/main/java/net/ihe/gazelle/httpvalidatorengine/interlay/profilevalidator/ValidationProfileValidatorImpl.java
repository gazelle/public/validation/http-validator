package net.ihe.gazelle.httpvalidatorengine.interlay.profilevalidator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.*;
import jakarta.json.bind.JsonbBuilder;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.ValidationProfileException;
import net.ihe.gazelle.httpvalidatorengine.application.profilevalidator.ValidationProfileValidator;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileAdapter;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ValidationProfileValidatorImpl implements ValidationProfileValidator {

    private final Logger logger = LoggerFactory.getLogger(ValidationProfileValidatorImpl.class);
    private static final String JSON_SCHEMA_PATH = "jsonSchema/ValidationProfileSchema.json";
    private final ValidationProfileAdapter adapter;

    public ValidationProfileValidatorImpl(ValidationProfileAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public ValidationProfile validate(String jsonString) {
        validateJsonSchema(jsonString, loadJSonSchema());
        ValidationProfile validationProfile = adapter.adaptFromString(jsonString);
        if (!validationProfile.isValid()) {
            throw new ValidationProfileException("ERROR : The ValidationProfile is not valid.");
        }
        validateAssertionIdsAreUnique(validationProfile.getAssertions());
        validateEL(validationProfile.getAssertions());
        adapter.adaptToString(validationProfile);
        return validationProfile;
    }

    private void validateJsonSchema(String jsonString, String jsonSchemaString) {
        List<String> errorMessages;
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonData = mapper.readTree(jsonString);
            JsonNode schemaNode = mapper.readTree(jsonSchemaString);
            JsonSchemaFactory factory = JsonSchemaFactory.getInstance(SpecVersionDetector.detect(schemaNode));
            JsonSchema jsonSchema = factory.getSchema(jsonSchemaString);
            Set<ValidationMessage> errors = jsonSchema.validate(jsonData);
            errorMessages = errors.stream()
                    .map(ValidationMessage::getMessage)
                    .toList();
        } catch (JsonProcessingException e) {
            logger.error("validateJsonSchema(jsonString) : Unable to process jsonString");
            throw new ValidationProfileException(e.getMessage(), e.getCause());
        }
        if (!errorMessages.isEmpty()) {
            throw new ValidationProfileException(JsonbBuilder.create().toJson(errorMessages));
        }
    }

    private String loadJSonSchema() {
        return FileUtils.readFileFromName(JSON_SCHEMA_PATH);
    }

    private void validateEL(List<Assertion> assertions) {
        for (Assertion assertion : assertions) {
            String expression = assertion.getSelector();
            String applyIf = assertion.getApplyIf();
            for (String blackListed : ELExpressionBlackList.getValueList()) {
                if (expression.toLowerCase().contains(blackListed.toLowerCase())) {
                    logger.error("validateEL(assertions) : The Selector contains a blacklisted String.");
                    throw new ValidationProfileException("The selector in Assertion " + assertion.getId() + " is not valid.");
                }
                if (applyIf != null && applyIf.toLowerCase().contains(blackListed.toLowerCase())) {
                    logger.error("validateEL(assertions) : The applyIf contains a blacklisted String.");
                    throw new ValidationProfileException("The applyIf in Assertion " + assertion.getId() + " is not valid.");
                }
            }
        }
    }

    private void validateAssertionIdsAreUnique(List<Assertion> assertions) {
        List<String> idList = assertions.stream().map(Assertion::getId).toList();
        Set<String> uniqueIdSet = new HashSet<>();
        for (String id : idList) {
            if (!uniqueIdSet.add(id)) {
                throw new ValidationProfileException("The id " + id + " is already used in this ValidationProfile");
            }
        }
    }

}
