package net.ihe.gazelle.httpvalidatorengine.interlay.factory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.AssertionDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileDTO;

@ApplicationScoped
public class DTOFactory {

    private CheckDTOFactory checkDTOFactory;

    public DTOFactory() {
    }

    @Inject
    public DTOFactory(CheckDTOFactory checkDTOFactory) {
        this.checkDTOFactory = checkDTOFactory;
    }

    public ValidationProfileDTO createValidationProfileDTO() {
        return new ValidationProfileDTO().setDTOFactory(this);
    }

    public AssertionDTO createAssertionDTO() {
        return new AssertionDTO().setCheckDTOFactory(checkDTOFactory);
    }
}
