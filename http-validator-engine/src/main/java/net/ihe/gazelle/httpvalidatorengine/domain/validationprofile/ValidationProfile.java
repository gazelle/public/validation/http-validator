package net.ihe.gazelle.httpvalidatorengine.domain.validationprofile;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ValidationProfile implements Serializable {

    @Serial
    private static final long serialVersionUID = -4272746532162706340L;
    private ProfileType profileType;
    private String id;
    private String name;
    private String description;
    private String context;
    private List<Assertion> assertions;

    public ProfileType getProfileType() {
        return profileType;
    }

    public ValidationProfile setProfileType(ProfileType profileType) {
        this.profileType = profileType;
        return this;
    }

    public String getId() {
        return id;
    }

    public ValidationProfile setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public ValidationProfile setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public ValidationProfile setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getContext() {
        return context;
    }

    public ValidationProfile setContext(String context) {
        this.context = context;
        return this;
    }

    public List<Assertion> getAssertions() {
        return new ArrayList<>(assertions);
    }

    public ValidationProfile setAssertions(List<Assertion> assertions) {
        this.assertions = new ArrayList<>(assertions);
        return this;
    }

    public boolean isValid() {
        return this.profileType != null &&
                this.id != null && !this.id.isEmpty() && !this.id.isBlank() &&
                this.name != null && !this.name.isEmpty() && !this.name.isBlank() &&
                this.context != null && !this.context.isEmpty() && !this.context.isBlank() &&
                this.assertions != null && !this.assertions.isEmpty() && isAssertionListValid();
    }

    private boolean isAssertionListValid() {
        for (Assertion assertion : assertions) {
            if (!assertion.isValid()) {
                return false;
            }
        }
        return true;
    }
}
