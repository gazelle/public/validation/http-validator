package net.ihe.gazelle.httpvalidatorengine.domain.httpmodel;

import java.util.List;
import java.util.Objects;

public class HeaderValue {

    private String value;
    private final ParameterList parameterList;

    public HeaderValue() {
        this(null, null);
    }

    public HeaderValue(String value) {
        this(value, null);
    }

    public HeaderValue(String value, ParameterList parameterList) {
        this.value = value;
        this.parameterList = Objects.requireNonNullElseGet(parameterList, ParameterList::new);
    }

    public String getValue() {
        return value;
    }

    public HeaderValue setValue(String value) {
        this.value = value;
        return this;
    }

    public ParameterList getParameterList() {
        return this.parameterList;
    }

    public void addAllParameters(ParameterList parameterList) {
        this.parameterList.addAll(parameterList.getParameters());
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this.value != null) {
            stringBuilder.append(this.value);
        }
        if (!this.parameterList.getParameters().isEmpty()) {
            stringBuilder.append(";");
            List<String> parameterStrings = this.parameterList.getParameters()
                    .stream()
                    .map(Parameter::toString)
                    .toList();
            stringBuilder.append(String.join(";", parameterStrings));
        }
        return stringBuilder.toString();
    }

    ParameterList parameters(String parameterName) {
        return new ParameterList(parameterList.getParameters().stream().filter(
                parameter -> parameter.getName().equals(parameterName)
        ).toList());
    }
}
