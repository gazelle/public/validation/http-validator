package net.ihe.gazelle.httpvalidatorengine.interlay.checkers.occurrence;

import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.json.bind.annotation.JsonbTransient;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.AbstractCheckDTO;

@JsonbPropertyOrder({"type", "minOccurrence", "maxOccurrence"})
public class OccurrenceCheckDTO extends AbstractCheckDTO {

    public static final String JSON_VALUE_TYPE = "OCCURRENCE";

    public OccurrenceCheckDTO() {
        super(new OccurrenceCheck());
    }

    @JsonbProperty(value = "minOccurrence")
    public int getMinOccurrence() {
        return getDomainObject().getMinOccurrence();
    }

    public OccurrenceCheckDTO setMinOccurrence(int minOccurrence) {
        getDomainObject().setMinOccurrence(minOccurrence);
        return this;
    }

    @JsonbProperty(value = "maxOccurrence")
    public int getMaxOccurrence() {
        return getDomainObject().getMaxOccurrence();
    }

    public OccurrenceCheckDTO setMaxOccurrence(int maxOccurrence) {
        getDomainObject().setMaxOccurrence(maxOccurrence);
        return this;
    }

    @JsonbTransient
    public OccurrenceCheck getDomainObject() {
        return (OccurrenceCheck) super.getCheck();
    }

    public OccurrenceCheckDTO setDomainObject(OccurrenceCheck domain) {
        super.setCheck(domain);
        return this;
    }
}
