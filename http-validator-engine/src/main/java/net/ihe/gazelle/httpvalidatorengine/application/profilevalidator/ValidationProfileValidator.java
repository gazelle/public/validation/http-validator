package net.ihe.gazelle.httpvalidatorengine.application.profilevalidator;


import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;

public interface ValidationProfileValidator {
    ValidationProfile validate(String jsonString);
}
