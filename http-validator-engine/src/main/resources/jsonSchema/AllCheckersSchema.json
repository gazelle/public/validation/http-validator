{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "type": "object",
    "required": ["profileType", "id", "name", "context", "assertions"],
    "properties": {
        "profileType": {
            "type": "string",
            "enum": ["HTTPREQUEST", "HTTPRESPONSE"]
        },
        "id": {
            "type": "string",
            "minLength": 1
        },
        "name": {
            "type": "string",
            "minLength": 1
        },
        "description": {
            "type": "string"
        },
        "context": {
            "type": "string",
            "minLength": 1
        },
        "referencedProfile": {
            "type": "array",
            "minItems": 0,
            "prefixItems": [
                {
                    "type": "string",
                    "minLength": 1
                }
            ]
        },
        "assertions": {
            "type": "array",
            "minItems": 1,
            "prefixItems": {
                "$ref": "#/$defs/assertion"
            }
        }
    },
    "$defs": {
        "assertion": {
            "type": "object",
            "required": ["selector", "id", "description", "requirementPriority", "checksComposition", "checks"],
            "unevaluatedProperties": false,
            "properties": {
                "selector": {
                    "type": "string",
                    "minLength": 1,
                    "pattern": "^(?!.*\\$.*|.*\\{.*|.*\\}.*)(?:.[^\\.]+\\.){0,9}.[^\\.]+$"
                },
                "id": {
                    "type": "string",
                    "minLength": 1
                },
                "description": {
                    "type": "string",
                    "minLength": 1
                },
                "requirementPriority": {
                    "type": "string",
                    "enum": ["MANDATORY", "RECOMMENDED", "PERMITTED"]
                },
                "applyIf": {
                    "type": "string",
                    "minLength": 1,
                    "pattern": "^(?!.*\\$.*|.*\\{.*|.*\\}.*)(?:.[^\\.]+\\.){0,9}.[^\\.]+$"
                },
                "checksComposition": {
                    "type": "string",
                    "enum": ["oneOf", "allOf", "anyOf", "not"]
                },
                "checks": {
                    "type": "array",
                    "minItems": 1,
                    "prefixItems": {
                        "$ref": "#/$defs/check"
                    }
                }
            }
        },
        "check": {
            "type": "object",
            "properties": {
                "type": {
                    "type": "string",
                    "enum": ["FIXEDVALUE", "REGEX", "CLOSEDLIST", "OCCURRENCE", "DATATYPE", "VALUESET", "RANGE"]
                }
            },
            "required": ["type"],
            "unevaluatedProperties": false,
            "allOf": [
                {
                    "if": {"properties": {"type": {"const": "FIXEDVALUE"}}},
                    "then": {"$ref": "#/$defs/fixedValueCheck"}
                },
                {
                    "if": {"properties": {"type": {"const": "REGEX"}}},
                    "then": {"$ref": "#/$defs/regexCheck"}
                },
                {
                    "if": {"properties": {"type": {"const": "CLOSEDLIST"}}},
                    "then": {"$ref": "#/$defs/closedListCheck"}
                },
                {
                    "if": {"properties": {"type": {"const": "OCCURRENCE"}}},
                    "then": {"$ref": "#/$defs/occurrenceCheck"}
                },
                {
                    "if": {"properties": {"type": {"const": "DATATYPE"}}},
                    "then": {"$ref": "#/$defs/dataTypeCheck"}
                },
                {
                    "if": {"properties": {"type": {"const": "VALUESET"}}},
                    "then": {"$ref": "#/$defs/valueSetCheck"}
                },
                {
                    "if": {"properties": {"type": {"const": "RANGE"}}},
                    "then": {"$ref": "#/$defs/rangeCheck"}
                }
            ]
        },
        "fixedValueCheck": {
            "type": "object",
            "required": ["fixedValue"],
            "properties": {
                "fixedValue": {"type": "string", "minLength": 1}
            },
            "minProperties": 2,
            "maxProperties": 2
        },
        "regexCheck": {
            "type": "object",
            "required": ["regex"],
            "properties": {
                "regex": {"type": "string", "minLength": 1}
            },
            "minProperties": 2,
            "maxProperties": 2
        },
        "closedListCheck": {
            "type": "object",
            "required": ["values"],
            "properties": {
                "values": {
                    "type": "array",
                    "minItems": 1,
                    "prefixItems": {
                        "type": "string",
                        "minLength": 1
                    }
                }
            },
            "minProperties": 2,
            "maxProperties": 2
        },
        "occurrenceCheck": {
            "type": "object",
            "required": ["minOccurrence", "maxOccurrence"],
            "properties": {
                "minOccurrence": {"type": "integer"},
                "maxOccurrence": {"type": "integer"}
            },
            "minProperties": 3,
            "maxProperties": 3
        },
        "dataTypeCheck": {
            "type": "object",
            "required": ["datatype"],
            "properties": {
                "datatype": {"type": "string", "minLength": 1}
            },
            "minProperties": 2,
            "maxProperties": 2
        },
        "valueSetCheck": {
            "type": "object",
            "required": ["oid"],
            "properties": {
                "oid": {
                    "type": "string",
                    "minLength": 1,
                    "pattern": "^\\d+(\\.\\d+)*$"
                }
            },
            "minProperties": 2,
            "maxProperties": 2
        },
        "rangeCheck": {
            "type": "object",
            "required": ["minRange", "maxRange"],
            "properties": {
                "minRange": {"type": "number"},
                "maxRange": {"type": "number"}
            },
            "minProperties": 3,
            "maxProperties": 3
        }
    }
}