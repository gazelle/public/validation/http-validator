package application.engine;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbConfig;
import mocks.MockDomainHTTP;
import net.ihe.gazelle.httpvalidatorengine.application.engine.ELEngine;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.EngineException;
import net.ihe.gazelle.httpvalidatorengine.domain.httpmodel.Request;
import net.ihe.gazelle.httpvalidatorengine.domain.httpmodel.Response;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.AssertionDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.validationreport.HttpAssertionResultHandler;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.FileUtils;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationSubReport;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ELEngineTest {

    private final Request request = MockDomainHTTP.HTTP_REQUEST_VALID_HEADERS;
    private final Response response = MockDomainHTTP.HTTP_RESPONSE;
    private final ELEngine ELEngine = new ELEngine();
    private static Assertion assertionOK;
    private static Assertion assertionKO;
    private static HttpAssertionResultHandler handler;

    @BeforeAll
    static void setup() {
        String assertionJsonOK = FileUtils.readFileFromName("ok/Assertion.json");
        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withFormatting(true));
        assertionOK = jsonb.fromJson(assertionJsonOK, AssertionDTO.class).getDomainObject();

        String assertionJsonKO = FileUtils.readFileFromName("ko/AssertionSelector_ko.json");
        assertionKO = jsonb.fromJson(assertionJsonKO, AssertionDTO.class).getDomainObject();
        ValidationSubReport validationSubReport = new ValidationSubReport().setName("Unit Test");
        handler = new HttpAssertionResultHandler(validationSubReport);
    }

    @AfterEach
    void tearDown() {
        assertionOK.setSelector("request.method");
    }

    @Test
    void okSelectorEngineTest() {
        assertNotNull(assertionOK);
        assertDoesNotThrow(() -> ELEngine.select(request, assertionOK, handler));
        List<String> results = ELEngine.select(request, assertionOK, handler);
        assertEquals("GET", results.get(0));
    }

    @Test
    void selectorEngineErrorTest() {
        assertNotNull(assertionKO);
        assertThrows(EngineException.class, () -> ELEngine.select(request, assertionKO, handler));
    }

    @Test
    void okELExpressionTest() {
        String expression = "request.method";
        assertionOK.setSelector(expression);
        List<String> results = ELEngine.select(request, assertionOK, handler);
        assertEquals("GET", results.get(0));

        expression = "request.uri.path";
        assertionOK.setSelector(expression);
        results = ELEngine.select(request, assertionOK, handler);
        assertEquals("/wado/rs", results.get(0));

        expression = "request.uri.queryParams('id').values";
        assertionOK.setSelector(expression);
        results = ELEngine.select(request, assertionOK, handler);
        assertEquals(1, results.size());
        assertEquals("WIAJean", results.get(0));

        expression = "response.statusCode";
        assertionOK.setSelector(expression);
        results = ELEngine.select(response, assertionOK, handler);
        assertEquals("200", results.get(0));

        expression = "response.statusMessage";
        assertionOK.setSelector(expression);
        results = ELEngine.select(response, assertionOK, handler);
        assertEquals("OK", results.get(0));

        expression = "response.version";
        assertionOK.setSelector(expression);
        results = ELEngine.select(response, assertionOK, handler);
        assertEquals("HTTP/1.1", results.get(0));

        expression = "response.headers('Content-Type').values";
        assertionOK.setSelector(expression);
        results = ELEngine.select(response, assertionOK, handler);
        assertEquals(1, results.size());
        assertEquals("application/dicom", results.get(0));
    }

    @Test
    void notExistingHeaderTest() {
        String expression = "request.headers('Test')";
        assertionOK.setSelector(expression);
        List<String> results = ELEngine.select(request, assertionOK, handler);
        assertEquals(1,results.size());
    }

    @Test
    void listExpressionTest() {
        String expression = "request.headers('Accept').values";
        assertionOK.setSelector(expression);
        List<String> results = ELEngine.select(request, assertionOK, handler);
        assertEquals(3, results.size());
        expression = "request.headers('Accept').parameters('type').values";
        assertionOK.setSelector(expression);
        results = ELEngine.select(request, assertionOK, handler);
        assertEquals(3, results.size());
    }

    @Test
    void unsupportedExpressionType() {
        String expression = "request.headers('Accept')";
        assertionOK.setSelector(expression);
        List<String> testUnsupported = ELEngine.select(request, assertionOK, handler);
        assertEquals(1, testUnsupported.size());
    }

    @Test
    void okApplyIfTest() {
        assertTrue(ELEngine.applyIf(request, assertionOK, handler));
        String trueApplyIf = "request.headers('Accept').values.size() == 3";
        assertionOK.setApplyIf(trueApplyIf);
        assertTrue(ELEngine.applyIf(request, assertionOK, handler));
        String falseApplyIf = "request.headers('Accept').values.size() == 2";
        assertionOK.setApplyIf(falseApplyIf);
        assertFalse(ELEngine.applyIf(request, assertionOK, handler));
        String test = "!request.uri.path.contains('instances')";
        assertionOK.setApplyIf(test);
        assertTrue(ELEngine.applyIf(request, assertionOK, handler));
        String superior = "request.uri.queryParams('resource').values.size() > 0";
        assertionOK.setApplyIf(superior);
        assertFalse(ELEngine.applyIf(request, assertionOK, handler));
    }

    @Test
    void koApplyIfTest() {
        String applyIf = "request.headers('Accept').values";
        assertionOK.setApplyIf(applyIf);
        assertThrows(EngineException.class, () -> ELEngine.applyIf(request, assertionOK, handler));
    }
}
