package interlay.profilevalidator;

import jakarta.json.bind.JsonbBuilder;
import mocks.MockDTO;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.ValidationProfileException;
import net.ihe.gazelle.httpvalidatorengine.application.profilevalidator.ValidationProfileValidator;
import net.ihe.gazelle.httpvalidatorengine.interlay.profilevalidator.ELExpressionBlackList;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.FileUtils;
import org.junit.jupiter.api.Test;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ValidationProfileValidatorTest {

    private final ValidationProfileValidator validator = MockDTO.createValidationProfileValidatorImpl();

    @Test
    void ok_ValidateJsonFormatTest() {
        String profileJson = FileUtils.readFileFromName("ok/ValidationProfile.json");
        assertDoesNotThrow(() -> validator.validate(profileJson));
    }

    @Test
    void ko_ValidationProfileValidatorTest() {
        String profileJson = FileUtils.readFileFromName("ko/ValidationProfile2.json");
        assertDoesNotThrow(MockDTO::createValidationProfileValidatorImpl);
        assertThrows(ValidationProfileException.class, () -> validator.validate(profileJson));
    }

    @Test
    void ko_ValidateJsonSchemaTest() {
        String profileJson = FileUtils.readFileFromName("ko/ValidationProfile2.json");
        List<String> errors = analyseErrors(profileJson);
        assertTrue(errors.size() > 0);
    }

    @Test
    void blackListELGetValuesTest() {
        assertEquals(ELExpressionBlackList.values().length, ELExpressionBlackList.getValueList().size());
    }

    @Test
    void ko_SelectorTest() {
        String profileJson = FileUtils.readFileFromName("ko/ValidationProfile3.json");
        assertThrows(ValidationProfileException.class, () -> validator.validate(profileJson));
        try {
            validator.validate(profileJson);
        } catch (ValidationProfileException e) {
            assertTrue(e.getMessage().contains("not valid"));
        }
    }

    private List<String> analyseErrors(String profileJson) {
        List<String> errors = new ArrayList<>();
        try {
            validator.validate(profileJson);
        } catch (ValidationProfileException e) {
            errors = JsonbBuilder.create().fromJson(e.getMessage(), new ArrayList<String>() {
                @Serial private static final long serialVersionUID = -605471799924772731L;
            }.getClass().getGenericSuperclass());
        }
        return errors;
    }
}
