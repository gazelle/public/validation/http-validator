package interlay.utils;

import net.ihe.gazelle.httpvalidatorengine.interlay.utils.EngineMessage;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.FileUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class FileUtilTest {

    @Test
    void okReadFileNameTest() {
        String test = FileUtils.readFileFromName("ok/ValidationProfile.json");
        assertNotNull(test);
    }

    @Test
    void koReadFileNameTest() {
        assertThrows(IllegalStateException.class, () -> FileUtils.readFileFromName("ok/test.json"));
    }

    @Test
    void okReadFromStreamTest() throws IOException {
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("ok/ValidationProfile.json");
        assertDoesNotThrow(() ->  FileUtils.readJsonFromStream(is));
        String test =  FileUtils.readJsonFromStream(is);
        assertNotNull(test);
    }

    @Test
    void okLoadPropertiesFromResourceTest() {
        Properties properties = FileUtils.loadPropertiesFromResource("engineMessages.properties");
        assertNotNull(properties);
        assertNotNull(EngineMessage.SELECTOR_MAL_FORMED.getFormattedValue());
    }

    @Test
    void koLoadPropertiesFromResourceTest() {
        Properties properties = FileUtils.loadPropertiesFromResource("test.json");
        assertNotNull(properties);
        assertTrue(properties.isEmpty());
    }

    @Test
    void okExtractJsonStringFromZipTest() {
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("ok/ValidationProfileBatch.zip");
        Map<String, String> fileContents = FileUtils.extractJsonStringFromZip(is);
        assertEquals(2, fileContents.size());
    }

    @Test
    void koExtractJsonStringFromZipTest() {
        assertThrows(IllegalStateException.class, () -> FileUtils.extractJsonStringFromZip(null));
    }
}
