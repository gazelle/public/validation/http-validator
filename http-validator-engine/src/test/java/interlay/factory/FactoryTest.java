package interlay.factory;

import mocks.MockDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.fixedvalue.FixedValueCheckDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.fixedvalue.FixedValueChecker;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.fixedvalue.FixedValueCheck;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.AssertionDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileDTO;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FactoryTest {

    @Test
    void checkDTOFactoryTest() {
        FixedValueCheck check = new FixedValueCheck();
        FixedValueCheckDTO checkDTO = (FixedValueCheckDTO) MockDTO.checkDTOFactory.create(check);
        assertEquals(check.getFixedValue(), checkDTO.getFixedValue());
    }

    @Test
    void dtoAssertionFactoryTest() {
        Assertion assertion = new Assertion();
        AssertionDTO assertionDTO = MockDTO.dtoFactory.createAssertionDTO();
        assertEquals(assertion.getId(), assertionDTO.getId());
    }

    @Test
    void dtoValidationProfileFactoryTest() {
        ValidationProfile profile = new ValidationProfile();
        ValidationProfileDTO validationProfileDTO = MockDTO.dtoFactory.createValidationProfileDTO();
        assertEquals(profile.getId(), validationProfileDTO.getId());
    }

    @Test
    void checkerFactoryTest() {
        Check check = new FixedValueCheck();
        assertTrue(MockDTO.checkerFactory.create(check) instanceof FixedValueChecker);
    }
}
