package interlay.dto;

import mocks.MockDTO;
import mocks.MockDomainProfile;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.ValidationProfileException;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.AssertionDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileDTO;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidationProfileDTOTest {

    MockDomainProfile domainMock = new MockDomainProfile();

    @Test
    void ok_ValidationProfileFromJsonDTOTest() throws ValidationProfileException {
        ValidationProfileDTO dto = MockDTO.createValidationProfileDTO(domainMock.createHTTPRequestValidationProfile_OK());
        ValidationProfile domain = dto.getDomainObject();
        assertEquals(dto.getProfileType(), domain.getProfileType());
        assertEquals(dto.getId(), domain.getId());
        assertEquals(dto.getDescription(), domain.getDescription());
        assertEquals(dto.getName(), domain.getName());
        assertEquals(dto.getContext(), domain.getContext());
        assertEquals(dto.getAssertions().size(), dto.getAssertions().size());
    }

    @Test
    void ok_AssertionFromJsonDTOTest() throws ValidationProfileException {
        AssertionDTO dto = MockDTO.createAssertionDTO(domainMock.createHTTPMethodAssertion_OK());
        Assertion domain = dto.getDomainObject();
        assertEquals(dto.getSelector(), domain.getSelector());
        assertEquals(dto.getId(), domain.getId());
        assertEquals(dto.getDescription(), domain.getDescription());
        assertEquals(dto.getRequirementPriority(), domain.getRequirementPriority());
        assertEquals(dto.getChecksComposition(), domain.getChecksComposition());
        assertEquals(dto.getChecks().size(), dto.getChecks().size());
    }
}
