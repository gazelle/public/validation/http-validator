package interlay.dto;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbConfig;
import mocks.MockDTO;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.closedlist.ClosedListCheck;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.fixedvalue.FixedValueCheck;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.occurrence.OccurrenceCheck;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.regex.RegexCheck;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.AbstractCheckDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.AssertionDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.FileUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SerializeDeserializeFileTest {


    @Test
    void ok_FixedValueCheckerSerializeDeserializeTest() {
        String test = FileUtils.readFileFromName("ok/FixedValueCheck.json");
        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withFormatting(true));
        Check check = jsonb.fromJson(test, AbstractCheckDTO.class).getCheck();
        String test2 = jsonb.toJson(MockDTO.createFixedValueCheckerDTO((FixedValueCheck) check));
        assertEquals(test, test2);
    }

    @Test
    void ko_FixedValueCheckerSerializeDeserializeTest() {
        String test = FileUtils.readFileFromName("ko/FixedValueChecker.json");
        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withFormatting(true));
        Check check = jsonb.fromJson(test, AbstractCheckDTO.class).getCheck();
        String test2 = jsonb.toJson(MockDTO.createFixedValueCheckerDTO((FixedValueCheck) check));
        assertNotEquals(test, test2);
    }

    @Test
    void ok_RegexCheckerSerializeDeserializeTest() {
        String test = FileUtils.readFileFromName("ok/RegexCheck.json");
        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withFormatting(true));
        Check check = jsonb.fromJson(test, AbstractCheckDTO.class).getCheck();
        String test2 = jsonb.toJson(MockDTO.createRegexCheckerDTO((RegexCheck) check));
        assertEquals(test, test2);
    }

    @Test
    void ko_RegexCheckerSerializeDeserializeTest() {
        String test = FileUtils.readFileFromName("ko/RegexChecker.json");
        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withFormatting(true));
        Check check = jsonb.fromJson(test, AbstractCheckDTO.class).getCheck();
        String test2 = jsonb.toJson(MockDTO.createRegexCheckerDTO((RegexCheck) check));
        assertNotEquals(test, test2);
    }

    @Test
    void ok_ClosedListCheckerSerializeDeserializeTest() {
        String test = FileUtils.readFileFromName("ok/ClosedListCheck.json");
        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withFormatting(true));
        Check check = jsonb.fromJson(test, AbstractCheckDTO.class).getCheck();
        String test2 = jsonb.toJson(MockDTO.createClosedListCheckerDTO((ClosedListCheck) check));
        assertEquals(test, test2);
    }

    @Test
    void ok_OccurrenceCheckerSerializeDeserializeTest() {
        String test = FileUtils.readFileFromName("ok/OccurrenceCheck.json");
        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withFormatting(true));
        Check check = jsonb.fromJson(test, AbstractCheckDTO.class).getCheck();
        String test2 = jsonb.toJson(MockDTO.createOccurrenceCheckerDTO((OccurrenceCheck) check));
        assertEquals(test, test2);
    }

    @Test
    void ok_AssertionSerializeDeserializeTest() {
        String test = FileUtils.readFileFromName("ok/Assertion.json");
        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withFormatting(true));
        Assertion assertion = jsonb.fromJson(test, AssertionDTO.class).getDomainObject();
        String test2 = jsonb.toJson(MockDTO.createAssertionDTO(assertion));
        assertEquals(test, test2);
    }

    @Test
    void ko_AssertionSerializeDeserializeTest() {
        String test = FileUtils.readFileFromName("ko/Assertion.json");
        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withFormatting(true));
        Assertion assertion = jsonb.fromJson(test, AssertionDTO.class).getDomainObject();
        String test2 = jsonb.toJson(MockDTO.createAssertionDTO(assertion));
        assertNotEquals(test, test2);
    }

    @Test
    void ok_ValidationProfileSerializeDeserializeTest() {
        String test = FileUtils.readFileFromName("ok/ValidationProfile.json");
        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withFormatting(true));
        ValidationProfile profile = jsonb.fromJson(test, ValidationProfileDTO.class).getDomainObject();
        String test2 = jsonb.toJson(MockDTO.createValidationProfileDTO(profile));
        assertEquals(test, test2);
    }

    @Test
    void ko_ValidationSerializeDeserializeTest() {
        String test = FileUtils.readFileFromName("ko/ValidationProfile.json");
        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withFormatting(true));
        ValidationProfile profile = jsonb.fromJson(test, ValidationProfileDTO.class).getDomainObject();
        String test2 = jsonb.toJson(MockDTO.createValidationProfileDTO(profile));
        assertNotEquals(test, test2);
    }
}
