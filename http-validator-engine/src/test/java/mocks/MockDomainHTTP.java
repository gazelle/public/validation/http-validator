package mocks;

import net.ihe.gazelle.httpvalidatorengine.domain.httpmodel.Request;
import net.ihe.gazelle.httpvalidatorengine.domain.httpmodel.Response;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ProfileType;
import net.ihe.gazelle.httpvalidatorengine.interlay.parser.HttpMessageParser;

public class MockDomainHTTP {

    private static final String LINE_SEPARATOR = "\r\n";
    public static final HttpMessageParser MESSAGE_PARSER = new HttpMessageParser();
    public static final Request HTTP_REQUEST_VALID_HEADERS = createHttpRequestValidHeaders();
    public static final Request HTTP_REQUEST_VALID_NO_HEADERS = createHttpRequestValidNoHeaders();
    public static final Request HTTP_REQUEST_MULTI = createHttpRequestMulti();
    public static final Response HTTP_RESPONSE = createHttpResponseValid();

    public static String createHTTPRequestString() {
        return "GET /wado/rs?id=WIAJean HTTP/1.1" + LINE_SEPARATOR +
                "Host: central-archive.ihe-europe.net" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.70;q=0.9;boundary=**," +
                "multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.80;q=0.8;boundary=**," +
                "multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.5;q=0.6;boundary=**" +
                LINE_SEPARATOR + LINE_SEPARATOR;
    }

    private static Request createHttpRequestValidHeaders() {
        String httpRequestValidHeaders = "GET /wado/rs?id=WIAJean HTTP/1.1" + LINE_SEPARATOR +
                "Host: central-archive.ihe-europe.net" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.70;q=0.9;boundary=**," +
                "multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.80;q=0.8;boundary=**," +
                "multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.5;q=0.6;boundary=**" +
                LINE_SEPARATOR + LINE_SEPARATOR;
        return (Request) MESSAGE_PARSER.parse(httpRequestValidHeaders, ProfileType.HTTPREQUEST);
    }

    private static Request createHttpRequestValidNoHeaders() {
        String httpRequestValidNoHeader = "GET https://central-archive.ihe-europe.net/wado/rs?id=WIAJean HTTP/1.1" + LINE_SEPARATOR +
                LINE_SEPARATOR;
        return (Request) MESSAGE_PARSER.parse(httpRequestValidNoHeader, ProfileType.HTTPREQUEST);
    }

    private static Request createHttpRequestMulti() {
        String httpRequestMulti =  "GET /wado/rs?id=WIAJean HTTP/1.1" + LINE_SEPARATOR +
                "Host: central-archive.ihe-europe.net" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.70;q=0.9;boundary=**" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.80;q=0.8;boundary=**" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.5;q=0.6;boundary=**" + LINE_SEPARATOR +
                LINE_SEPARATOR;
        return (Request) MESSAGE_PARSER.parse(httpRequestMulti, ProfileType.HTTPREQUEST);
    }

    private static Response createHttpResponseValid() {
        String httpResponse =  "HTTP/1.1 200 OK" + LINE_SEPARATOR +
                "Content-Type: application/dicom" + LINE_SEPARATOR +
                LINE_SEPARATOR;
        return (Response) MESSAGE_PARSER.parse(httpResponse, ProfileType.HTTPRESPONSE);
    }
}
