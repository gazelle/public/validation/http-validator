package mocks;

import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.closedlist.ClosedListCheck;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.fixedvalue.FixedValueCheck;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.occurrence.OccurrenceCheck;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.regex.RegexCheck;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ProfileType;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.validation.api.domain.report.structure.RequirementPriority;

import java.util.ArrayList;
import java.util.List;

public class MockDomainProfile {

    public FixedValueCheck createFixedValueCheck_OK() {
        return new FixedValueCheck().setFixedValue("GET");
    }

    public FixedValueCheck createFixedValueCheck_KO() {
        return new FixedValueCheck().setFixedValue(null);
    }

    public RegexCheck createRegexCheck_OK() {
        return new RegexCheck().setRegex("^(\\/[^\\/\\s]+)+$");
    }

    public RegexCheck createRegexCheck_KO() {
        return new RegexCheck().setRegex(null);
    }

    public OccurrenceCheck createOccurrenceCheck_OK() {
        return new OccurrenceCheck().setMinOccurrence(1).setMaxOccurrence(2);
    }

    public OccurrenceCheck createOccurrenceCheck_KO() {
        return new OccurrenceCheck().setMinOccurrence(3).setMaxOccurrence(2);
    }

    public ClosedListCheck createClosedListCheck_OK() {
        List<String> closedList = new ArrayList<>();
        closedList.add("first");
        closedList.add("second");
        return new ClosedListCheck().setValues(closedList);
    }

    public ClosedListCheck createClosedListCheck_KO() {
        List<String> closedList = new ArrayList<>();
        return new ClosedListCheck().setValues(closedList);
    }

    public List<Check> createFixedCheckList() {
        List<Check> checks = new ArrayList<>();
        checks.add(createFixedValueCheck_OK());
        checks.add(createOccurrenceCheck_OK());
        return checks;
    }

    public List<Check> createRegexCheckList() {
        List<Check> checks = new ArrayList<>();
        checks.add(createRegexCheck_OK());
        return checks;
    }

    public Assertion createHTTPMethodAssertion_OK() {
        return new Assertion().setSelector("request.method")
                .setId("GETMethodChecking").setDescription("Assertion for the HTTP method checking")
                .setRequirementPriority(RequirementPriority.MANDATORY)
                .setChecksComposition("oneOf").setChecks(createFixedCheckList());
    }

    public Assertion createHTTPMethodAssertion_KO() {
        return new Assertion().setSelector(null)
                .setId("GETMethodChecking").setDescription("Assertion for the HTTP method checking")
                .setRequirementPriority(RequirementPriority.MANDATORY)
                .setChecksComposition("oneOf").setChecks(createFixedCheckList());
    }

    public Assertion createHTTPVersionAssertion() {
        return new Assertion().setSelector("request.version")
                .setId("HTTP1VersionChecking").setDescription("Assertion for the HTTP version checking")
                .setRequirementPriority(RequirementPriority.MANDATORY)
                .setChecksComposition("oneOf").setChecks(createFixedCheckList());
    }

    public Assertion createRequestPathAssertion() {
        return new Assertion().setSelector("request.uri.path")
                .setId("URIRegexChecking").setDescription("Assertion for the HTTP uri checking")
                .setRequirementPriority(RequirementPriority.MANDATORY)
                .setChecksComposition("oneOf").setChecks(createRegexCheckList());
    }

    public List<Assertion> createAssertionList() {
        List<Assertion> assertions = new ArrayList<>();
        assertions.add(createHTTPMethodAssertion_OK());
        assertions.add(createHTTPVersionAssertion());
        assertions.add(createRequestPathAssertion());
        return assertions;
    }

    public ValidationProfile createHTTPRequestValidationProfile_OK() {
        return new ValidationProfile().setProfileType(ProfileType.HTTPREQUEST)
                .setId("WADO-RS_Request_Validation_Profile").setName("WADO_HTTPRequest")
                .setDescription("Validation Profile for validating an WADO-RS HTTP Request")
                .setContext("DIM").setAssertions(createAssertionList());
    }

    public ValidationProfile createHTTPRequestValidationProfile_KO() {
        return new ValidationProfile().setProfileType(null)
                .setId("WADO-RS_Request_Validation_Profile").setName("WADO_HTTPRequest")
                .setDescription("Validation Profile for validating an WADO-RS HTTP Request")
                .setContext("DIM").setAssertions(createAssertionList());
    }

    public ValidationProfile createHTTPResponseValidationProfile() {
        return new ValidationProfile().setProfileType(ProfileType.HTTPRESPONSE)
                .setId("WADO-RS_Response_Validation_Profile").setName("WADO_HTTPResponse")
                .setDescription("Validation Profile for validating an WADO-RS HTTP Response")
                .setContext("DIM").setAssertions(createAssertionList());
    }
}
