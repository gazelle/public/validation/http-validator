package domain.validationprofile;

import mocks.MockDomainProfile;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ProfileType;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidationProfileTest {

    MockDomainProfile domainMock = new MockDomainProfile();

    @Test
    void ok_httpRequestProfileTest() {
        ValidationProfile profile = domainMock.createHTTPRequestValidationProfile_OK();
        assertTrue(profile.isValid());
        assertEquals(ProfileType.HTTPREQUEST, profile.getProfileType());
        assertEquals("WADO-RS_Request_Validation_Profile", profile.getId());
        assertEquals("WADO_HTTPRequest", profile.getName());
        assertEquals("Validation Profile for validating an WADO-RS HTTP Request", profile.getDescription());
        assertEquals("DIM", profile.getContext());
        assertEquals(3, profile.getAssertions().size());
    }

    @Test
    void ko_httpRequestProfileTest() {
        ValidationProfile profile = domainMock.createHTTPRequestValidationProfile_KO();
        assertFalse(profile.isValid());
    }

    @Test
    void ok_httpResponseProfileTest() {
        ValidationProfile profile = domainMock.createHTTPResponseValidationProfile();
        assertTrue(profile.isValid());
        assertEquals(ProfileType.HTTPRESPONSE, profile.getProfileType());
        assertEquals(3, profile.getAssertions().size());
    }

}
