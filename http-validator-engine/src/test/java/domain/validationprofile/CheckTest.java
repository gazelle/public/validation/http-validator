package domain.validationprofile;

import mocks.MockDomainProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.closedlist.ClosedListCheck;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.fixedvalue.FixedValueCheck;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.occurrence.OccurrenceCheck;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.regex.RegexCheck;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CheckTest {

    MockDomainProfile domainMock = new MockDomainProfile();

    @Test
    void ok_fixedValueCheckTest() {
        FixedValueCheck check = domainMock.createFixedValueCheck_OK();
        assertTrue(check.isValid());
        assertEquals("GET", check.getFixedValue());
        assertNotEquals("POST", check.getFixedValue());
    }

    @Test
    void ko_fixedValueCheckTest() {
        FixedValueCheck check = domainMock.createFixedValueCheck_KO();
        assertFalse(check.isValid());
    }

    @Test
    void ok_regexCheckTest() {
        RegexCheck check = domainMock.createRegexCheck_OK();
        assertTrue(check.isValid());
        assertEquals("^(\\/[^\\/\\s]+)+$", check.getRegex());
    }

    @Test
    void ko_regexCheckTest() {
        RegexCheck check = domainMock.createRegexCheck_KO();
        assertFalse(check.isValid());
    }

    @Test
    void ok_closedListCheckTest() {
        ClosedListCheck check = domainMock.createClosedListCheck_OK();
        assertTrue(check.isValid());
        assertEquals(2, check.getValues().size());
    }

    @Test
    void ko_closedListCheckTest() {
        ClosedListCheck check = domainMock.createClosedListCheck_KO();
        assertFalse(check.isValid());
    }

    @Test
    void ok_occurrenceCheckTest() {
        OccurrenceCheck check = domainMock.createOccurrenceCheck_OK();
        assertTrue(check.isValid());
        assertEquals(1, check.getMinOccurrence());
        assertEquals(2, check.getMaxOccurrence());
    }

    @Test
    void ko_occurrenceCheckTest() {
        OccurrenceCheck check = domainMock.createOccurrenceCheck_KO();
        assertFalse(check.isValid());
    }
}
