package domain.httpmodel;

import mocks.MockDomainHTTP;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.SelectorException;
import net.ihe.gazelle.httpvalidatorengine.domain.httpmodel.*;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ProfileType;
import net.ihe.gazelle.httpvalidatorengine.interlay.parser.HttpMessageParser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ModelTest {

    private final HttpMessageParser messageParser = MockDomainHTTP.MESSAGE_PARSER;
    private final Request httpRequestValidHeaders = MockDomainHTTP.HTTP_REQUEST_VALID_HEADERS;
    private final Request httpRequestValidNoHeader = MockDomainHTTP.HTTP_REQUEST_VALID_NO_HEADERS;
    private final Request httpRequestMulti = MockDomainHTTP.HTTP_REQUEST_MULTI;
    private final Response httpResponse = MockDomainHTTP.HTTP_RESPONSE;

    @Test
    void okRequestTest() {
        assertEquals("GET", httpRequestValidHeaders.getMethod());
        assertNotNull(httpRequestValidHeaders.getUri());
        assertEquals("HTTP/1.1", httpRequestValidHeaders.getVersion());
        assertNotNull(httpRequestValidHeaders.getMessageHeaders());
        assertEquals(2, httpRequestValidHeaders.getMessageHeaders().size());
        String requestString = httpRequestValidHeaders.toString();
        Request request = (Request) messageParser.parse(requestString, ProfileType.HTTPREQUEST);
        String requestString2 = request.toString();
        assertEquals(requestString, requestString2);
    }

    @Test
    void okTestURI() {
        URIModel uriModel = httpRequestValidHeaders.getUri();
        assertEquals("/wado/rs", uriModel.getPath());
        assertEquals(1, uriModel.getQueryParams().getValues().size());
        assertNotNull(uriModel.getQueryParams().getValues().get(0));
    }

    @Test
    void okTestQueryParam() {
        URIModel uriModel = httpRequestValidHeaders.getUri();
        Parameter queryParam = uriModel.getQueryParams().getParameters().get(0);
        assertEquals("id", queryParam.getName());
        assertEquals("WIAJean", queryParam.getValue());
        queryParam = uriModel.queryParams("id").getParameters().get(0);
        assertNotNull(queryParam);
    }

    @Test
    void okHeaders() {
        HeaderModel acceptHeader = httpRequestValidHeaders.headers("Accept");
        assertEquals("Accept", httpRequestValidHeaders.getMessageHeaders().get(1).getName());
        assertEquals("Accept", acceptHeader.getName());
        assertEquals(0,
                httpRequestValidHeaders.headers("notExist").getValues().size());
        assertNotNull(acceptHeader.getValues());
        assertEquals(3, acceptHeader.getValues().size());
        HeaderValue firstValue = acceptHeader.getValues().get(0);
        assertEquals("multipart/related", firstValue.getValue());
        assertNotNull(firstValue.getParameterList());
        assertEquals(4, firstValue.getParameterList().getValues().size());
        assertEquals("type", firstValue.getParameterList().getParameters().get(0).getName());
    }

    @Test
    void okRequestNoHeaderTest() {
        assertEquals("GET", httpRequestValidNoHeader.getMethod());
        assertNotNull(httpRequestValidNoHeader.getUri());
        assertEquals("HTTP/1.1", httpRequestValidNoHeader.getVersion());
        assertNotNull(httpRequestValidNoHeader.getMessageHeaders());
        assertEquals(1, httpRequestValidNoHeader.getMessageHeaders().size());
        assertEquals( 0,httpRequestValidNoHeader.headers("Accept").getValues().size());
    }

    @Test
    void okRequestMultiAcceptTest() {
        assertEquals("GET", httpRequestMulti.getMethod());
        assertNotNull(httpRequestMulti.getUri());
        assertEquals("HTTP/1.1", httpRequestMulti.getVersion());
        assertNotNull(httpRequestMulti.getMessageHeaders());
        assertEquals(2, httpRequestMulti.getMessageHeaders().size());
    }

    @Test
    void okResponseTest() {
        assertEquals("HTTP/1.1", httpResponse.getVersion());
        assertEquals(200, httpResponse.getStatusCode());
        assertEquals("OK", httpResponse.getStatusMessage());
        assertNotNull(httpResponse.getMessageHeaders());
        assertEquals(1, httpResponse.getMessageHeaders().size());
        assertEquals("Content-Type", httpResponse.headers("Content-Type").getName());
        assertEquals("application/dicom", httpResponse.headers("Content-Type").getValues().get(0).getValue());
    }
}
