package interlay.ws;

import net.ihe.gazelle.httpvalidator.interlay.ws.HTTPValidatorApplication;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HTTPValidatorApplicationTest {

    @Test
    void getClassesTest() {
        HTTPValidatorApplication application = new HTTPValidatorApplication();
        assertEquals(3, application.getClasses().size());
    }
}
