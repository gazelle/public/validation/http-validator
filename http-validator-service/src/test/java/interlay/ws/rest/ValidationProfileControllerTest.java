package interlay.ws.rest;

import jakarta.json.bind.JsonbBuilder;
import jakarta.ws.rs.core.EntityPart;
import jakarta.ws.rs.core.Response;
import mocks.MockDTO;
import mocks.MockDomainProfile;
import net.ihe.gazelle.httpvalidator.application.exceptions.ProfileNotFoundException;
import net.ihe.gazelle.httpvalidator.application.service.ValidationProfileService;
import net.ihe.gazelle.httpvalidator.interlay.service.ServiceMessage;
import net.ihe.gazelle.httpvalidator.interlay.ws.rest.importprofile.ValidationProfileController;
import net.ihe.gazelle.httpvalidator.interlay.ws.rest.importprofile.ValidationProfileControllerImpl;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.ValidationProfileException;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileAdapter;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ValidationProfileControllerTest {

    private static ValidationProfileService serviceMock;
    private static ValidationProfileAdapter adapterMock;
    private static ValidationProfileController controller;
    private final MockDomainProfile mock = new MockDomainProfile();

    @BeforeAll
    static void setUp() throws Exception {
        serviceMock = mock(ValidationProfileService.class);
        adapterMock = mock(ValidationProfileAdapter.class);
        controller = new ValidationProfileControllerImpl();
        Field serviceField = ValidationProfileControllerImpl.class.getDeclaredFields()[0];
        Field adapterField = ValidationProfileControllerImpl.class.getDeclaredFields()[1];
        serviceField.setAccessible(true);
        serviceField.set(controller, serviceMock);
        adapterField.setAccessible(true);
        adapterField.set(controller, adapterMock);
    }

    @Test
    void okGetValidationProfileTest() {
        String id = "some-id";
        ValidationProfile expectedProfile = mock.createHTTPRequestValidationProfile_OK();
        ValidationProfileAdapter adapter = MockDTO.createAdapterImpl();
        String json = adapter.adaptToString(expectedProfile);
        when(serviceMock.getAvailableProfile(id)).thenReturn(expectedProfile);
        when(adapterMock.adaptToString(expectedProfile)).thenReturn(json);
        Response response = controller.getValidationProfile(id);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals(json, response.getEntity());
    }

    @Test
    void koGetValidationProfileTest() {
        doThrow(new RuntimeException("error")).when(serviceMock).getAvailableProfile(anyString());
        Response response = controller.getValidationProfile("some-id");
        assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
    }

    @Test
    void okImportValidationProfileTest() {
        ValidationProfile profile = mock.createHTTPRequestValidationProfile_OK();
        ValidationProfileAdapter adapter = MockDTO.createAdapterImpl();
        String json = adapter.adaptToString(profile);
        when(serviceMock.validateProfile(json)).thenReturn(profile);
        doNothing().when(serviceMock).importProfile(profile);
        Response response = controller.importValidationProfile(json);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals(ServiceMessage.VALID_PROFILE.getFormattedValue(profile.getId()), response.getEntity());
    }

    @Test
    void koImportValidationProfileTest() {
        ValidationProfile profile = mock.createHTTPRequestValidationProfile_OK();
        ValidationProfileAdapter adapter = MockDTO.createAdapterImpl();
        String json = adapter.adaptToString(profile);
        ValidationProfileException exception = new ValidationProfileException("invalid profile");
        when(serviceMock.validateProfile(json)).thenReturn(profile);
        doThrow(exception).when(serviceMock).importProfile(profile);
        Response response = controller.importValidationProfile(json);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertEquals("invalid profile", response.getEntity());
    }

    @Test
    void okImportValidationProfilesTest() {
        EntityPart part = mock(EntityPart.class);
        InputStream inputStream = mock(InputStream.class);
        when(part.getContent()).thenReturn(inputStream);

        List<EntityPart> parts = List.of(part);
        List<String> expectedBatchResult = List.of("result");
        when(serviceMock.importProfileBatch(inputStream)).thenReturn(expectedBatchResult);

        Response response = controller.importValidationProfiles(parts);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals(JsonbBuilder.create().toJson(expectedBatchResult), response.getEntity());
    }

    @Test
    void koImportValidationProfilesTest() {
        EntityPart part = mock(EntityPart.class);
        InputStream inputStream = mock(InputStream.class);
        when(part.getContent()).thenReturn(inputStream);

        List<EntityPart> parts = List.of(part);
        ValidationProfileException exception = new ValidationProfileException("invalid profile");
        doThrow(exception).when(serviceMock).importProfileBatch(inputStream);

        Response response = controller.importValidationProfiles(parts);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertEquals("invalid profile", response.getEntity());
    }

    @Test
    void okDeleteValidationProfileTest() {
        String id = "some-id";
        doNothing().when(serviceMock).deleteProfile(id);

        Response response = controller.deleteValidationProfile(id);

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals(ServiceMessage.DELETE_SUCCESSFUL.getFormattedValue(id), response.getEntity());
    }

    @Test
    void koDeleteValidationProfileTest() {
        String id = "some-id";
        ProfileNotFoundException exception = new ProfileNotFoundException("Not found");
        doThrow(exception).when(serviceMock).deleteProfile(id);

        Response response = controller.deleteValidationProfile(id);

        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
        assertEquals("Not found", response.getEntity());
    }
}
