package interlay.dao;

import net.ihe.gazelle.httpvalidator.application.dao.ProfileMetadata;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProfileMetadataTest {

    @Test
    void testProfileMetadata() {
        ProfileMetadata profileMetadata = new ProfileMetadata("HTTPREQUEST", "id", "name", "description", "context", "path");
        assertEquals("HTTPREQUEST", profileMetadata.getProfileType());
        assertEquals("id", profileMetadata.getId());
        assertEquals("name", profileMetadata.getName());
        assertEquals("description", profileMetadata.getDescription());
        assertEquals("context", profileMetadata.getContext());
        assertEquals("path", profileMetadata.getPath());
    }
}
