package interlay.dao;

import mocks.MockDTO;
import net.ihe.gazelle.httpvalidator.application.dao.ProfileMetadata;
import net.ihe.gazelle.httpvalidator.application.exceptions.ProfileNotFoundException;
import net.ihe.gazelle.httpvalidator.application.config.Preferences;
import net.ihe.gazelle.httpvalidator.interlay.config.PreferencesEnvImpl;
import net.ihe.gazelle.httpvalidator.interlay.dao.ValidationProfileDAOCacheImpl;
import net.ihe.gazelle.httpvalidator.interlay.dao.ValidationProfileDAOImpl;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.ValidationProfileException;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileAdapter;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ValidationProfileDAOTest {

    private static ValidationProfile validationProfile;
    private static ValidationProfileDAOCacheImpl daoCache;
    private static final String MOCK_PROFILE_REPOSITORY_PATH = "src/test/resources/fileSystemUT/";

    @BeforeAll
    static void setUp() {
        Preferences preferences = mock(PreferencesEnvImpl.class);
        ValidationProfileAdapter adapter = MockDTO.createAdapterImpl();
        validationProfile = adapter.adaptFromString(FileUtils.readFileFromName("ok/ValidationProfile.json"));
        ValidationProfileDAOImpl dao = new ValidationProfileDAOImpl(preferences, adapter);
        daoCache = new ValidationProfileDAOCacheImpl(preferences, dao);
        when(preferences.getValidationProfileRepositoryPath()).thenReturn(MOCK_PROFILE_REPOSITORY_PATH);
        when(preferences.getMaxCacheElements()).thenReturn(1);
        dao.init();
    }

    @BeforeEach
    void init() {
        daoCache.init();
    }

    @AfterEach
    public void tearDown() throws IOException {
        clearDirectory();
    }

    @Test
    void saveValidationProfileTest() {
        List<ValidationProfile> profiles = daoCache.getAllProfiles();
        assertNotNull(profiles);
        assertEquals(0, profiles.size());
        daoCache.saveValidationProfile(validationProfile);
        profiles = new ArrayList<>(daoCache.getAllProfiles());
        assertNotNull(profiles);
        assertEquals(1, profiles.size());
        assertEquals(validationProfile.getId(), profiles.get(0).getId());
    }

    @Test
    void getProfileMetadataTest() {
        daoCache.saveValidationProfile(validationProfile);
        List<ProfileMetadata> profileMetadataList = daoCache.getProfileMetadata();
        assertNotNull(profileMetadataList);
        assertEquals(1, profileMetadataList.size());
        assertEquals(validationProfile.getId(), profileMetadataList.get(0).getId());
        assertEquals(validationProfile.getName(), profileMetadataList.get(0).getName());
    }

    @Test
    void saveProfileAlreadyUsedIDErrorTest() {
        daoCache.saveValidationProfile(validationProfile);
        assertThrows(ValidationProfileException.class, () -> daoCache.saveValidationProfile(validationProfile));
        try {
            daoCache.saveValidationProfile(validationProfile);
        } catch (ValidationProfileException e) {
            assertTrue(e.getMessage().contains("already used"));
        }
    }

    @Test
    void getAllProfilesTest() {
        daoCache.saveValidationProfile(validationProfile);
        List<ValidationProfile> profiles = new ArrayList<>(daoCache.getAllProfiles());
        assertNotNull(profiles);
        assertEquals(1, profiles.size());
        ValidationProfile validationProfileTest = profiles.get(0);
        assertEquals(validationProfile.getId(), validationProfileTest.getId());
        assertEquals(validationProfile.getName(), validationProfileTest.getName());
        assertEquals(validationProfile.getAssertions().size(), validationProfileTest.getAssertions().size());
        assertEquals(validationProfile.getAssertions().get(0).getId(), validationProfileTest.getAssertions().get(0).getId());
        assertEquals(validationProfile.getAssertions().get(0).getSelector(), validationProfileTest.getAssertions().get(0).getSelector());
        assertEquals(validationProfile.getAssertions().get(0).getChecks().size(), validationProfileTest.getAssertions().get(0).getChecks().size());
        assertEquals(validationProfile.getAssertions().get(0).getChecks().get(0).getClass(), validationProfileTest.getAssertions().get(0).getChecks().get(0).getClass());
    }

    @Test
    void getValidationProfileByIDTest() {
        daoCache.saveValidationProfile(validationProfile);
        ValidationProfile validationProfileTest = daoCache.getValidationProfileByID(validationProfile.getId());
        assertEquals(validationProfile.getId(), validationProfileTest.getId());
        assertEquals(validationProfile.getName(), validationProfileTest.getName());
        assertEquals(validationProfile.getAssertions().size(), validationProfileTest.getAssertions().size());
        assertEquals(validationProfile.getAssertions().get(0).getId(), validationProfileTest.getAssertions().get(0).getId());
        assertEquals(validationProfile.getAssertions().get(0).getChecks().size(), validationProfileTest.getAssertions().get(0).getChecks().size());
        assertEquals(validationProfile.getAssertions().get(0).getChecks().get(0).getClass(), validationProfileTest.getAssertions().get(0).getChecks().get(0).getClass());
    }

    @Test
    void getValidationProfileByIDNotExistingErrorTest() {
        daoCache.saveValidationProfile(validationProfile);
        ValidationProfile validationProfileTest = daoCache.getValidationProfileByID(validationProfile.getId());
        assertNotNull(validationProfileTest);
        assertThrows(ProfileNotFoundException.class, () -> daoCache.getValidationProfileByID("bad_id"));
        try {
            daoCache.getValidationProfileByID("bad_id");
        } catch (ProfileNotFoundException e) {
            assertTrue(e.getMessage().contains("Unable to find ValidationProfile with id"));
        }
    }

    @Test
    void getProfilePathByIdTest() {
        daoCache.saveValidationProfile(validationProfile);
        Path path = daoCache.getProfilePathById(validationProfile.getId());
        assertNotNull(path);
        assertEquals(MOCK_PROFILE_REPOSITORY_PATH+validationProfile.getId()+".json", path.toString());
    }

    @Test
    void deleteValidationProfileTest() {
        daoCache.saveValidationProfile(validationProfile);
        List<ValidationProfile> profiles = daoCache.getAllProfiles();
        assertNotNull(profiles);
        assertEquals(1, profiles.size());
        daoCache.deleteValidationProfile(validationProfile.getId());
        profiles = new ArrayList<>(daoCache.getAllProfiles());
        assertNotNull(profiles);
        assertEquals(0, profiles.size());
    }

    @Test
    void deleteNotExistingErrorTest() {
        String id = validationProfile.getId();
        assertThrows(ProfileNotFoundException.class, () -> daoCache.deleteValidationProfile(id));
        try {
            daoCache.deleteValidationProfile(id);
        } catch (ProfileNotFoundException e) {
            assertTrue(e.getMessage().contains("Unable to find ValidationProfile"));
        }
    }

    private void clearDirectory() throws IOException {
        File directory = new File(MOCK_PROFILE_REPOSITORY_PATH);
        final File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (!file.isDirectory() && file.getPath().contains(".json")) {
                    Files.delete(Path.of(file.getPath()));
                }
            }
        }
    }
}
