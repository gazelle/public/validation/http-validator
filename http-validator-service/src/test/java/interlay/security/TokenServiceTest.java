package interlay.security;

import net.ihe.gazelle.gazelletokenclient.application.AuthenticationClient;
import net.ihe.gazelle.gazelletokenclient.domain.IdentityToken;
import net.ihe.gazelle.httpvalidator.application.config.Preferences;
import net.ihe.gazelle.httpvalidator.application.security.TokenService;
import net.ihe.gazelle.httpvalidator.interlay.security.TokenServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TokenServiceTest {

    private static Preferences preferencesMock;
    private static AuthenticationClient clientMock;
    private static TokenService tokenService;

    @BeforeAll
    static void setUp() throws Exception {
        preferencesMock = mock(Preferences.class);
        clientMock = mock(AuthenticationClient.class);
        tokenService = new TokenServiceImpl();
        Field preferenceField = TokenServiceImpl.class.getDeclaredFields()[0];
        Field clientField = TokenServiceImpl.class.getDeclaredFields()[1];
        preferenceField.setAccessible(true);
        preferenceField.set(tokenService, preferencesMock);
        clientField.setAccessible(true);
        clientField.set(tokenService, clientMock);
    }

    @Test
    void callGazelleTokenAPITest() {
        String mock_token = "mock_token";
        when(preferencesMock.getTokenApiUrl()).thenReturn("https://example.com");
        when(clientMock.requestGazelleTokenService(anyString(), eq(mock_token))).thenReturn(createMockToken());
        IdentityToken identityToken = tokenService.callGazelleTokenAPI(mock_token);
        assertEquals("token", identityToken.getValue());
        assertEquals("KEREVAL", identityToken.getOrganization());
        assertEquals("username", identityToken.getUsername());
        assertEquals(1, identityToken.getRoles().size());
        assertTrue(identityToken.isAdmin());
    }

    private IdentityToken createMockToken() {
        List<String> roles = new ArrayList<>();
        roles.add("admin_role");
        return IdentityToken.builder()
                .withValue("token")
                .withCreationDate(new Date())
                .withExpirationDate(new Date())
                .withOrganization("KEREVAL")
                .withUsername("username")
                .withRoles(roles)
                .build();
    }
}
