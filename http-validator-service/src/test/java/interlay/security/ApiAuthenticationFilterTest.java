package interlay.security;

import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.core.HttpHeaders;
import mock.MockDomainToken;
import net.ihe.gazelle.gazelletokenclient.domain.IdentityToken;
import net.ihe.gazelle.gazelletokenclient.interlay.exceptions.ExpiredTokenException;
import net.ihe.gazelle.httpvalidator.interlay.security.ApiAuthenticationFilter;
import net.ihe.gazelle.httpvalidator.application.security.TokenService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class ApiAuthenticationFilterTest {

    private static HttpServletRequest requestMock;
    private static HttpServletResponse responseMock;
    private static FilterChain filterChainMock;
    private static TokenService tokenServiceMock;
    private static ApiAuthenticationFilter filter;
    private static IdentityToken identityToken;

    @BeforeAll
    static void setUp() throws Exception {
        requestMock = mock(HttpServletRequest.class);
        responseMock = mock(HttpServletResponse.class);
        filterChainMock = mock(FilterChain.class);
        tokenServiceMock = mock(TokenService.class);
        filter = new ApiAuthenticationFilter();
        Field tokenServiceField = ApiAuthenticationFilter.class.getDeclaredFields()[0];
        tokenServiceField.setAccessible(true);
        tokenServiceField.set(filter, tokenServiceMock);
        identityToken = MockDomainToken.createAdminMockIdentityToken();
        doNothing().when(filterChainMock).doFilter(any(), any());
    }

    @Test
    void okDoFilterTest() {
        when(requestMock.getHeader("Authorization")).thenReturn("GazelleApiKey mock_token");
        when(tokenServiceMock.callGazelleTokenAPI(anyString())).thenReturn(identityToken);
        assertDoesNotThrow(() -> filter.doFilter(requestMock, responseMock, filterChainMock));
        verify(requestMock).setAttribute(anyString(), any(IdentityToken.class));
    }

    @Test
    void tokenExpiredDoFilterTest() throws IOException {
        when(requestMock.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn("GazelleApiKey expired-token");
        when(tokenServiceMock.callGazelleTokenAPI(anyString())).thenThrow(ExpiredTokenException.class);
        assertDoesNotThrow(() -> filter.doFilter(requestMock, responseMock, filterChainMock));
        verify(responseMock).sendError(401, "Token expired");
    }

    @Test
    void tokenInvalidDoFilterTest() throws IOException {
        when(requestMock.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn("");
        assertDoesNotThrow(() -> filter.doFilter(requestMock, responseMock, filterChainMock));
        verify(responseMock).sendError(401);
    }

    @Test
    void tokenBadSyntaxDoFilterTest() throws IOException {
        when(requestMock.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn("GazelleApi bad-syntax-token");
        assertDoesNotThrow(() -> filter.doFilter(requestMock, responseMock, filterChainMock));
        verify(responseMock).sendError(400);
    }

    @Test
    void tokenNotAdminDoFilterTest() throws IOException {
        HttpServletResponse responseMock = mock(HttpServletResponse.class);
        when(requestMock.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn("GazelleApiKey Not-admin");
        IdentityToken notAdminToken = MockDomainToken.createNotAdminMockIdentityToken();
        when(tokenServiceMock.callGazelleTokenAPI(anyString())).thenReturn(notAdminToken);
        assertDoesNotThrow(() -> filter.doFilter(requestMock, responseMock, filterChainMock));
        verify(responseMock).sendError(401);
    }
}
