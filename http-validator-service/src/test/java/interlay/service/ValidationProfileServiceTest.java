package interlay.service;

import mocks.MockDTO;
import net.ihe.gazelle.httpvalidator.application.dao.ProfileMetadata;
import net.ihe.gazelle.httpvalidator.application.exceptions.ProfileNotFoundException;
import net.ihe.gazelle.httpvalidator.application.service.ValidationProfileService;
import net.ihe.gazelle.httpvalidator.application.config.Preferences;
import net.ihe.gazelle.httpvalidator.interlay.config.PreferencesEnvImpl;
import net.ihe.gazelle.httpvalidator.interlay.dao.ValidationProfileDAOCacheImpl;
import net.ihe.gazelle.httpvalidator.interlay.dao.ValidationProfileDAOImpl;
import net.ihe.gazelle.httpvalidator.interlay.service.ServiceMessage;
import net.ihe.gazelle.httpvalidator.interlay.service.ValidationProfileServiceImpl;
import net.ihe.gazelle.httpvalidatorengine.application.profilevalidator.ValidationProfileValidator;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.ValidationProfileException;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileAdapter;
import net.ihe.gazelle.httpvalidatorengine.interlay.profilevalidator.ValidationProfileValidatorImpl;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class ValidationProfileServiceTest {

    private static ValidationProfile validationProfile;
    private static ValidationProfileServiceImpl service;
    private static ValidationProfileDAOCacheImpl daoCache;
    private static final String MOCK_PROFILE_REPOSITORY_PATH = "src/test/resources/fileSystemUT";

    @BeforeAll
    static void setup() {
        Preferences preferences = mock(PreferencesEnvImpl.class);
        ValidationProfileAdapter adapter = MockDTO.createAdapterImpl();
        ValidationProfileValidator validator = mock(ValidationProfileValidatorImpl.class);
        validationProfile = adapter.adaptFromString(FileUtils.readFileFromName("ok/ValidationProfile.json"));
        ValidationProfileDAOImpl dao = new ValidationProfileDAOImpl(preferences, adapter);
        daoCache = new ValidationProfileDAOCacheImpl(preferences, dao);
        service = new ValidationProfileServiceImpl(daoCache, validator);
        when(validator.validate(any(String.class))).thenReturn(validationProfile);
        when(preferences.getValidationProfileRepositoryPath()).thenReturn(MOCK_PROFILE_REPOSITORY_PATH);
        when(preferences.getMaxCacheElements()).thenReturn(1);
        dao.init();
    }

    @BeforeEach
    void init() {
        daoCache.init();
    }

    @AfterEach
    public void tearDown() throws IOException {
        clearDirectory();
    }

    @Test
    void serviceMessagePropFileTest() {
        Properties properties = FileUtils.loadPropertiesFromResource("serviceMessages.properties");
        assertNotNull(properties);
        assertNotNull(ServiceMessage.UNABLE_TO_SAVE.getFormattedValue());
    }

    @Test
    void importProfileTest() {
        List<ProfileMetadata> profileMetadataList = service.getProfiles();
        assertNotNull(profileMetadataList);
        assertEquals(0, profileMetadataList.size());
        service.importProfile(validationProfile);
        profileMetadataList = new ArrayList<>(service.getProfiles());
        assertNotNull(profileMetadataList);
        assertEquals(1, profileMetadataList.size());
        assertEquals(validationProfile.getId(), profileMetadataList.get(0).getId());
        assertEquals(validationProfile.getName(), profileMetadataList.get(0).getName());
    }

    @Test
    void importProfileAlreadyUsedIDErrorTest() {
        service.importProfile(validationProfile);
        assertThrows(ValidationProfileException.class, () -> service.importProfile(validationProfile));
        try {
            service.importProfile(validationProfile);
        } catch (ValidationProfileException e) {
            assertTrue(e.getMessage().contains("already used"));
        }
    }

    @Test
    void getProfilesTest() {
        service.importProfile(validationProfile);
        List<ProfileMetadata> profileMetadataList = service.getProfiles();
        assertNotNull(profileMetadataList);
        assertEquals(1, profileMetadataList.size());
        assertEquals(validationProfile.getId(), profileMetadataList.get(0).getId());
        assertEquals(validationProfile.getName(), profileMetadataList.get(0).getName());
    }

    @Test
    void deleteProfileTest() {
        service.importProfile(validationProfile);
        List<ProfileMetadata> profiles = service.getProfiles();
        assertNotNull(profiles);
        assertEquals(1, profiles.size());
        service.deleteProfile(validationProfile.getId());
        profiles = new ArrayList<>(service.getProfiles());
        assertNotNull(profiles);
        assertEquals(0, profiles.size());
    }

    @Test
    void deleteNotExistingErrorTest() {
        String id = validationProfile.getId();
        assertThrows(ProfileNotFoundException.class, () -> service.deleteProfile(id));
        try {
            service.deleteProfile(id);
        } catch (ProfileNotFoundException e) {
            assertTrue(e.getMessage().contains("Unable to find ValidationProfile"));
        }
    }

    @Test
    void importProfileBatchTest() {
        List<String> profiles = new ArrayList<>();
        String profileString = FileUtils.readFileFromName("ok/ValidationProfile.json");
        profiles.add(profileString);
        profiles.add(profileString);
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("ok/ValidationProfileBatch.zip");
        ValidationProfileService serviceMock = spy(service);
        doNothing().when(serviceMock).importProfile(validationProfile);
        List<String> results = serviceMock.importProfileBatch(is);
        assertEquals(profiles.size(), results.size());
    }

    private void clearDirectory() throws IOException {
        File directory = new File(ValidationProfileServiceTest.MOCK_PROFILE_REPOSITORY_PATH);
        final File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (!file.isDirectory() && file.getPath().contains(".json")) {
                    Files.delete(Path.of(file.getPath()));
                }
            }
        }
    }
}
