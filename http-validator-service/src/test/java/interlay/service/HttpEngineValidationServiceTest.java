package interlay.service;

import mocks.MockDTO;
import net.ihe.gazelle.httpvalidator.application.service.HttpEngineValidationService;
import net.ihe.gazelle.httpvalidator.interlay.service.HttpEngineValidationServiceImpl;
import net.ihe.gazelle.httpvalidatorengine.application.engine.Engine;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileAdapter;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.FileUtils;
import net.ihe.gazelle.validation.api.domain.report.structure.AssertionReport;
import net.ihe.gazelle.validation.api.domain.report.structure.RequirementPriority;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationSubReport;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationTestResult;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class HttpEngineValidationServiceTest {

    private static Engine engine;
    private static HttpEngineValidationService httpEngineValidationService;
    private static ValidationProfile validationProfile;

    @BeforeAll
    static void setup() throws IllegalAccessException {
        engine = mock(Engine.class);
        httpEngineValidationService = new HttpEngineValidationServiceImpl();
        Field engineFactoryField = HttpEngineValidationServiceImpl.class.getDeclaredFields()[0];
        engineFactoryField.setAccessible(true);
        engineFactoryField.set(httpEngineValidationService, engine);
        ValidationProfileAdapter adapter = MockDTO.createAdapterImpl();
        validationProfile = adapter.adaptFromString(FileUtils.readFileFromName("ok/ValidationProfile.json"));
    }

    @Test
    void okEngineServiceTest() {
        when(engine.validate(any(), any())).thenReturn(mockPassedReport());
        String request = "GET /authorize HTTP/1.1\n" + "Host: server.example.com";
        byte[] requestBytes = request.getBytes();
        ValidationSubReport validationSubReport = httpEngineValidationService.validate(requestBytes, validationProfile);
        assertEquals(1, validationSubReport.getAssertionReports().size());
        AssertionReport assertionReport = validationSubReport.getAssertionReports().get(0);
        assertEquals(ValidationTestResult.PASSED, assertionReport.getResult());
    }

    @Test
    void koEngineServiceTest() {
        when(engine.validate(any(), any())).thenReturn(mockFailedReport());
        String request = "GET /authorize HTTP/1.1\n" + "Host: server.example.com";
        byte[] requestBytes = request.getBytes();
        ValidationSubReport validationSubReport = httpEngineValidationService.validate(requestBytes, validationProfile);
        assertEquals(1, validationSubReport.getAssertionReports().size());
        AssertionReport assertionReport = validationSubReport.getAssertionReports().get(0);
        assertEquals(ValidationTestResult.FAILED, assertionReport.getResult());
    }

    private ValidationSubReport mockPassedReport() {
        return new ValidationSubReport()
                .setName("mock")
                .setStandards(new String[]{"HTTP"})
                .addAssertionReport(new AssertionReport()
                        .setAssertionID("method assertion")
                        .setDescription("Success.")
                        .setResult(ValidationTestResult.PASSED)
                        .setPriority(RequirementPriority.MANDATORY));
    }

    private ValidationSubReport mockFailedReport() {
        return new ValidationSubReport()
                .setName("mock")
                .setStandards(new String[]{"HTTP"})
                .addAssertionReport(new AssertionReport()
                        .setAssertionID("method assertion")
                        .setDescription("Failed.")
                        .setResult(ValidationTestResult.FAILED)
                        .setPriority(RequirementPriority.MANDATORY));
    }
}
