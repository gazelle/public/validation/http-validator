package interlay.service;

import net.ihe.gazelle.httpvalidator.application.service.WellFormedValidationService;
import net.ihe.gazelle.httpvalidator.interlay.service.WellFormedValidationServiceImpl;
import net.ihe.gazelle.httpvalidatorengine.application.engine.EngineFactory;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ProfileType;
import net.ihe.gazelle.validation.api.domain.report.structure.AssertionReport;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationSubReport;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationTestResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

class WellFormedValidationServiceTest {

    private WellFormedValidationService wellFormedValidationService;

    @BeforeEach
    void init() throws IllegalAccessException {
        EngineFactory engineFactory = new EngineFactory();
        wellFormedValidationService = new WellFormedValidationServiceImpl();
        Field engineFactoryField = WellFormedValidationServiceImpl.class.getDeclaredFields()[0];
        engineFactoryField.setAccessible(true);
        engineFactoryField.set(wellFormedValidationService, engineFactory);
    }

    @Test
    void okWellFormedTest() {
        String request = "GET /authorize HTTP/1.1\n" + "Host: server.example.com";
        byte[] requestBytes = request.getBytes();
        ValidationSubReport validationSubReport = wellFormedValidationService.validate(requestBytes, ProfileType.HTTPREQUEST);
        assertEquals(1, validationSubReport.getAssertionReports().size());
        AssertionReport assertionReport = validationSubReport.getAssertionReports().get(0);
        assertEquals(ValidationTestResult.PASSED, assertionReport.getResult());
        assertTrue(wellFormedValidationService.isWellFormed());
    }

    @Test
    void koWellFormedTest() {
        String request = "GET /authorize HTTP/1.1\n" + "Host server.example.com";
        byte[] requestBytes = request.getBytes();
        ValidationSubReport validationSubReport = wellFormedValidationService.validate(requestBytes, ProfileType.HTTPREQUEST);
        assertEquals(1, validationSubReport.getAssertionReports().size());
        AssertionReport assertionReport = validationSubReport.getAssertionReports().get(0);
        assertEquals(ValidationTestResult.FAILED, assertionReport.getResult());
        assertFalse(wellFormedValidationService.isWellFormed());
    }
}
