package net.ihe.gazelle.httpvalidator.interlay.security;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import net.ihe.gazelle.gazelletokenclient.application.AuthenticationClient;
import net.ihe.gazelle.httpvalidator.application.config.Preferences;
import net.ihe.gazelle.gazelletokenclient.domain.IdentityToken;
import net.ihe.gazelle.gazelletokenclient.interlay.exceptions.ExpiredTokenException;
import net.ihe.gazelle.gazelletokenclient.interlay.exceptions.InvalidTokenException;
import net.ihe.gazelle.httpvalidator.application.security.TokenService;

@ApplicationScoped
public class TokenServiceImpl implements TokenService {

    @Inject
    private Preferences preferences;
    @Inject
    private AuthenticationClient client;

    @Override
    public IdentityToken callGazelleTokenAPI(String tokenRequest) throws InvalidTokenException, ExpiredTokenException {
        return client.requestGazelleTokenService(preferences.getTokenApiUrl(), tokenRequest);
    }
}