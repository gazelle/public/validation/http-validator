package net.ihe.gazelle.httpvalidator.interlay.dao;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.Initialized;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import net.ihe.gazelle.httpvalidator.application.dao.ValidationProfileDAO;

@ApplicationScoped
public class DAOInitJob {

    @Inject
    ValidationProfileDAO validationProfileDAO;

    public void daoCacheInitAtStartup(@Observes @Initialized(ApplicationScoped.class) Object context) {
        validationProfileDAO.init();
    }

}
