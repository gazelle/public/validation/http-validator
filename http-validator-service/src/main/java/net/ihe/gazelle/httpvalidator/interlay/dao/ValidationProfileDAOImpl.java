package net.ihe.gazelle.httpvalidator.interlay.dao;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import net.ihe.gazelle.httpvalidator.application.exceptions.ProfileNotFoundException;
import net.ihe.gazelle.httpvalidator.application.dao.ProfileMetadata;
import net.ihe.gazelle.httpvalidator.application.dao.ValidationProfileDAO;
import net.ihe.gazelle.httpvalidator.application.config.Preferences;
import net.ihe.gazelle.httpvalidator.interlay.service.ServiceMessage;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.ValidationProfileException;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@DirectDAO
public class ValidationProfileDAOImpl implements ValidationProfileDAO {

    private final Logger logger = LoggerFactory.getLogger(ValidationProfileDAOImpl.class);
    private final Preferences preferences;
    private final ValidationProfileAdapter adapter;
    private File profileDirectory;
    private String profileDirectoryPath;
    private List<ProfileMetadata> profileMetadataList;

    @Inject
    public ValidationProfileDAOImpl(Preferences preferences, ValidationProfileAdapter adapter) {
        this.preferences = preferences;
        this.adapter = adapter;
    }

    @PostConstruct
    public void init() {
        logger.debug("DAO init");
        profileDirectoryPath = preferences.getValidationProfileRepositoryPath();
        profileDirectory = new File(profileDirectoryPath);
    }

    @Override
    public Preferences getPreferences() {
        return this.preferences;
    }

    @Override
    public List<ProfileMetadata> getProfileMetadata() {
        logger.debug("getProfileMetadata()");
        return new ArrayList<>(profileMetadataList);
    }

    @Override
    public List<ValidationProfile> getAllProfiles() {
        logger.debug("getAllProfiles()");
        List<ValidationProfile> validationProfiles = new ArrayList<>();
        File[] matchingFiles = profileDirectory.listFiles((d, name) -> name.endsWith(".json"));
        if (matchingFiles != null && matchingFiles.length > 0) {
            for (File file : matchingFiles) {
                validationProfiles.add(getProfileFromFile(file));
            }
        }
        updateMetadataList(validationProfiles);
        return validationProfiles;
    }

    @Override
    public ValidationProfile getValidationProfileByID(String id) {
        logger.debug("getValidationProfileByID({})", id);
        try {
            Path profilePath = getProfilePathById(id);
            String content = Files.readString(profilePath, StandardCharsets.UTF_8);
            return adapter.adaptFromString(content);
        } catch (IOException e) {
            throw new ValidationProfileException(ServiceMessage.UNABLE_TO_FIND.getFormattedValue(id), e.getCause());
        }
    }

    @Override
    public Path getProfilePathById(String id) {
        logger.debug("getProfilePathById({})", id);
        Optional<ProfileMetadata> optional = profileMetadataList.stream().filter(profileMetadata -> profileMetadata.getId().equals(id)).findFirst();
        if (optional.isPresent()) {
            return Paths.get(optional.get().getPath());
        } else {
            throw new ProfileNotFoundException(ServiceMessage.UNABLE_TO_FIND.getFormattedValue(id));
        }
    }

    @Override
    public void saveValidationProfile(ValidationProfile validationProfile) {
        logger.debug("saveValidationProfile({})", validationProfile.getId());
        String id = validationProfile.getId();
        assertIDNotUsed(id);
        try {
            Path newFile = Paths.get(new File(profileDirectoryPath, formatFileName(id)).getPath());
            Files.writeString(newFile, adapter.adaptToString(validationProfile));
            updateMetadata(validationProfile);
        } catch (IOException e) {
            throw new ValidationProfileException(ServiceMessage.UNABLE_TO_SAVE.getFormattedValue(), e.getCause());
        }
    }

    @Override
    public void deleteValidationProfile(String id) {
        logger.debug("deleteValidationProfile({})", id);
        try {
            Files.delete(getProfilePathById(id));
            Optional<ProfileMetadata> optional = profileMetadataList.stream().filter(profileMetadata -> profileMetadata.getId().equals(id)).findFirst();
            optional.ifPresent(profileMetadataFile -> profileMetadataList.remove(profileMetadataFile));
        } catch (IOException | ValidationProfileException e) {
            throw new ValidationProfileException(ServiceMessage.UNABLE_TO_FIND.getFormattedValue(id), e.getCause());
        }
    }

    public ValidationProfile getProfileFromFile(File file) {
        logger.debug("getProfileFromFile({})", file.getName());
        try {
            String content = Files.readString(Paths.get(file.getPath()), StandardCharsets.UTF_8);
            return adapter.adaptFromString(content);
        } catch (IOException e) {
            throw new ValidationProfileException(ServiceMessage.BAD_ID_FOR_GET.getFormattedValue(), e.getCause());
        }
    }

    private void updateMetadataList(List<ValidationProfile> validationProfiles) {
        profileMetadataList = new ArrayList<>();
        for (ValidationProfile validationProfile : validationProfiles) {
            updateMetadata(validationProfile);
        }
    }

    private void updateMetadata(ValidationProfile validationProfile) {
        profileMetadataList.add(new ProfileMetadata()
                .setProfileType(validationProfile.getProfileType().name())
                .setId(validationProfile.getId())
                .setName(validationProfile.getName())
                .setDescription(validationProfile.getDescription())
                .setContext(validationProfile.getContext())
                .setPath(profileDirectoryPath + "/" + validationProfile.getId() + ".json")
        );
    }

    private void assertIDNotUsed(String id) {
        if (profileMetadataList.stream().anyMatch(profileMetadataFile -> profileMetadataFile.getId().equals(id))) {
            throw new ValidationProfileException(ServiceMessage.ID_ALREADY_USED.getFormattedValue(id));
        }
    }

    public String formatFileName(String id) {
        return ServiceMessage.FILE_FORMAT_NAME.getFormattedValue(id);
    }
}
