package net.ihe.gazelle.httpvalidator.interlay.service;

import net.ihe.gazelle.httpvalidatorengine.interlay.utils.FileUtils;

import java.text.MessageFormat;
import java.util.Properties;

public enum ServiceMessage {

    UNABLE_TO_FIND,
    BAD_ID_FOR_GET,
    VALID_PROFILE,
    ID_ALREADY_USED,
    UNABLE_TO_SAVE,
    FILE_FORMAT_NAME,
    DELETE_SUCCESSFUL,
    INTERNAL_ERROR;

    private static final String MESSAGE_FILE_NAME = "serviceMessages.properties";

    private static final Properties PROPERTIES;

    static {
        PROPERTIES = FileUtils.loadPropertiesFromResource(MESSAGE_FILE_NAME);
    }

    public String getFormattedValue(Object ... arguments) {
        return MessageFormat.format(PROPERTIES.getProperty(this.name()), arguments);
    }
}
