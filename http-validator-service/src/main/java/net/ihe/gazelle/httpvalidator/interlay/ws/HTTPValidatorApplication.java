package net.ihe.gazelle.httpvalidator.interlay.ws;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;
import net.ihe.gazelle.httpvalidator.interlay.ws.rest.importprofile.ValidationProfileControllerImpl;
import net.ihe.gazelle.httpvalidator.interlay.ws.rest.validation.HttpMetadataApiWSImpl;
import net.ihe.gazelle.httpvalidator.interlay.ws.rest.validation.HttpValidationApiWSImpl;

import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/rest")
public class HTTPValidatorApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<>();
        s.add(ValidationProfileControllerImpl.class);
        s.add(HttpValidationApiWSImpl.class);
        s.add(HttpMetadataApiWSImpl.class);
        return s;
    }
}
