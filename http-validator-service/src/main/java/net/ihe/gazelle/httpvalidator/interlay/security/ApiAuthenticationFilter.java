package net.ihe.gazelle.httpvalidator.interlay.security;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.core.HttpHeaders;
import net.ihe.gazelle.gazelletokenclient.domain.IdentityToken;
import net.ihe.gazelle.gazelletokenclient.interlay.exceptions.ExpiredTokenException;
import net.ihe.gazelle.gazelletokenclient.interlay.exceptions.InvalidTokenException;
import net.ihe.gazelle.httpvalidator.application.security.TokenService;

import java.io.IOException;

@ApplicationScoped
public class ApiAuthenticationFilter implements Filter {

    @Inject
    private TokenService tokenService;
    public static final String CONST_TOKEN_ASSERTION = "_const_token_assertion_";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (!(request instanceof HttpServletRequest httpRequest)) {
            throw new ServletException("This filter can only process HttpServletRequest requests");
        } else {
            final HttpServletResponse httpResponse = (HttpServletResponse) response;

            try {
                String token = httpRequest.getHeader(HttpHeaders.AUTHORIZATION);
                assertAuthnScheme(token);
                IdentityToken identity = tokenService.callGazelleTokenAPI(token.substring(14));
                assertAdminRole(identity);
                httpRequest.setAttribute(CONST_TOKEN_ASSERTION, identity);
                chain.doFilter(httpRequest, httpResponse);
            } catch (ExpiredTokenException e) {
                httpResponse.sendError(401, "Token expired");
            } catch (InvalidTokenException e) {
                httpResponse.sendError(401);
            } catch (BadRequestException e) {
                httpResponse.sendError(400);
            }
        }
    }

    private void assertAuthnScheme(String token) {
        if (token == null || token.isBlank()) {
            throw new InvalidTokenException("Invalid token");
        }
        if (!token.startsWith("GazelleApiKey ")) {
            throw new BadRequestException("Bad syntax");
        }
    }

    private void assertAdminRole(IdentityToken identity) {
        if (!identity.isAdmin()) {
            throw new InvalidTokenException("Not admin");
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {
        // Initialization code here, if needed
    }

    @Override
    public void destroy() {
        // Cleanup code here, if needed
    }
}