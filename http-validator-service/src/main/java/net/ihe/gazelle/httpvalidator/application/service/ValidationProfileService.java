package net.ihe.gazelle.httpvalidator.application.service;

import net.ihe.gazelle.httpvalidator.application.config.Preferences;
import net.ihe.gazelle.httpvalidator.application.dao.ProfileMetadata;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;

import java.io.InputStream;
import java.util.List;

public interface ValidationProfileService {

    Preferences getPreferences();
    ValidationProfile validateProfile(String json);
    List<ProfileMetadata> getProfiles();
    ValidationProfile getAvailableProfile(String id);
    void importProfile(ValidationProfile validationProfile);
    List<String> importProfileBatch(InputStream is);
    void deleteProfile(String id);
}
