package net.ihe.gazelle.httpvalidator.interlay.config;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Default;
import net.ihe.gazelle.httpvalidator.application.config.Preferences;

@Default
@ApplicationScoped
public class PreferencesEnvImpl implements Preferences {

    public static final String APPLICATION_URL = "APPLICATION_URL";
    public static final String GZL_TOKEN_API_URL = "GZL_TOKEN_API_URL";
    public static final String GZL_PROFILE_REPOSITORY_PATH = "GZL_PROFILE_REPOSITORY_PATH";
    public static final String GZL_PROFILE_CACHE_MAX_ELEMENTS = "GZL_PROFILE_CACHE_MAX_ELEMENTS";
    private String applicationUrl;
    private String tokenApiUrl;
    private String validationProfileRepositoryPath;
    private Integer maxCacheElements;

    @PostConstruct
    public void init() {
        applicationUrl = assertNonNull(System.getenv(APPLICATION_URL), APPLICATION_URL);
        tokenApiUrl = assertNonNull(System.getenv(GZL_TOKEN_API_URL), GZL_TOKEN_API_URL);
        validationProfileRepositoryPath = assertNonNull(System.getenv(GZL_PROFILE_REPOSITORY_PATH), GZL_PROFILE_REPOSITORY_PATH);
        maxCacheElements = Integer.parseInt(assertNonNull(System.getenv(GZL_PROFILE_CACHE_MAX_ELEMENTS), GZL_PROFILE_CACHE_MAX_ELEMENTS));
    }

    private String assertNonNull(String value, String name) {
        if (value != null && !value.isBlank()) {
            return value;
        } else {
            throw new IllegalStateException("Missing environment variable " + name);
        }
    }

    @Override
    public String getApplicationUrl() {
        return applicationUrl;
    }

    @Override
    public String getTokenApiUrl() {
        return tokenApiUrl;
    }

    @Override
    public String getValidationProfileRepositoryPath() {
        return validationProfileRepositoryPath;
    }

    @Override
    public Integer getMaxCacheElements() {
        return maxCacheElements;
    }
}
