package net.ihe.gazelle.httpvalidator.interlay.factory;

import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import net.ihe.gazelle.framework.modelvalidator.interlay.adapter.BValidatorBuilderFactory;
import net.ihe.gazelle.httpvalidator.application.service.HttpEngineValidationService;
import net.ihe.gazelle.httpvalidator.application.service.ValidationProfileService;
import net.ihe.gazelle.httpvalidator.application.service.WellFormedValidationService;
import net.ihe.gazelle.httpvalidator.interlay.service.HttpValidationServiceImpl;
import net.ihe.gazelle.httpvalidator.interlay.service.ValidationMetadata;
import net.ihe.gazelle.servicemetadata.api.application.MetadataService;
import net.ihe.gazelle.validation.api.application.ValidationService;

public class HttpValidationServiceFactory {

    @Inject
    ValidationProfileService validationProfileService;
    @Inject
    WellFormedValidationService wellFormedValidationService;
    @Inject
    HttpEngineValidationService httpEngineValidationService;
    @Inject
    @ValidationMetadata
    MetadataService metadataService;

    @Produces
    public ValidationService getHttpValidationServiceInstance() {
        return new HttpValidationServiceImpl(
                validationProfileService,
                wellFormedValidationService,
                httpEngineValidationService,
                metadataService,
                new BValidatorBuilderFactory());
    }
}
