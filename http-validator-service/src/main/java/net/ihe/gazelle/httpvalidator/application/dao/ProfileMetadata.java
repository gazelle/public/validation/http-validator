package net.ihe.gazelle.httpvalidator.application.dao;

import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.json.bind.annotation.JsonbTransient;

@JsonbPropertyOrder({"profileType", "id", "name", "description", "context"})
public class ProfileMetadata {

    private String profileType;
    private String id;
    private String name;
    private String description;
    private String context;
    private String path;

    public ProfileMetadata() {
    }

    public ProfileMetadata(String profileType, String id, String name, String description, String context, String path) {
        this.profileType = profileType;
        this.id = id;
        this.name = name;
        this.description = description;
        this.context = context;
        this.path = path;
    }

    @JsonbProperty
    public String getId() {
        return id;
    }

    public ProfileMetadata setId(String id) {
        this.id = id;
        return this;
    }

    @JsonbProperty
    public String getName() {
        return name;
    }

    public ProfileMetadata setName(String name) {
        this.name = name;
        return this;
    }

    @JsonbProperty
    public String getProfileType() {
        return profileType;
    }

    public ProfileMetadata setProfileType(String profileType) {
        this.profileType = profileType;
        return this;
    }

    @JsonbProperty
    public String getDescription() {
        return description;
    }

    public ProfileMetadata setDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonbProperty
    public String getContext() {
        return context;
    }

    public ProfileMetadata setContext(String context) {
        this.context = context;
        return this;
    }

    @JsonbTransient
    public String getPath() {
        return path;
    }

    public ProfileMetadata setPath(String path) {
        this.path = path;
        return this;
    }
}
