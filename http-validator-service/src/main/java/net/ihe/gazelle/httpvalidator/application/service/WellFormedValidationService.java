package net.ihe.gazelle.httpvalidator.application.service;

import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ProfileType;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationSubReport;

public interface WellFormedValidationService {
    boolean isWellFormed();
    ValidationSubReport validate(byte[] contentByte, ProfileType profileType);
}
