package net.ihe.gazelle.httpvalidator.interlay.config;

import net.ihe.gazelle.httpvalidator.application.dao.ProfileMetadata;
import net.ihe.gazelle.validation.api.domain.metadata.structure.ValidationProfile;

import java.util.ArrayList;
import java.util.List;

public class ProfileConfiguration {

    private final List<ValidationProfile> validationProfiles;
    private final List<ProfileMetadata> profileMetadataList;

    public ProfileConfiguration(List<ProfileMetadata> profileMetadataList) {
        this.profileMetadataList = profileMetadataList;
        this.validationProfiles = new ArrayList<>();
        constructProfileList();
    }

    public List<ValidationProfile> getValidationProfiles() {
        return validationProfiles;
    }

    public void constructProfileList() {
        for (ProfileMetadata profileMetadata : profileMetadataList) {
            validationProfiles.add(new ValidationProfile()
                    .setProfileName(profileMetadata.getName())
                    .setProfileID(profileMetadata.getId())
                    .setDomain(profileMetadata.getContext()));
        }
    }
}
