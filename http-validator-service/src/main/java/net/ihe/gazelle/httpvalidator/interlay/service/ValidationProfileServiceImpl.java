package net.ihe.gazelle.httpvalidator.interlay.service;

import net.ihe.gazelle.httpvalidator.application.config.Preferences;
import net.ihe.gazelle.httpvalidator.application.dao.ProfileMetadata;
import net.ihe.gazelle.httpvalidator.application.dao.ValidationProfileDAO;
import net.ihe.gazelle.httpvalidator.application.service.ValidationProfileService;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.ValidationProfileException;
import net.ihe.gazelle.httpvalidatorengine.application.profilevalidator.ValidationProfileValidator;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.FileUtils;

import java.io.*;
import java.util.*;

public class ValidationProfileServiceImpl implements ValidationProfileService {

    private final ValidationProfileDAO validationProfileDAO;
    private final ValidationProfileValidator validationProfileValidator;

    public ValidationProfileServiceImpl(ValidationProfileDAO validationProfileDAO, ValidationProfileValidator validationProfileValidator) {
        this.validationProfileDAO = validationProfileDAO;
        this.validationProfileValidator = validationProfileValidator;
    }

    @Override
    public Preferences getPreferences() {
        return this.validationProfileDAO.getPreferences();
    }

    @Override
    public ValidationProfile validateProfile(String json) {
        return validationProfileValidator.validate(json);
    }

    @Override
    public List<ProfileMetadata> getProfiles() {
        return new ArrayList<>(validationProfileDAO.getProfileMetadata());
    }

    @Override
    public ValidationProfile getAvailableProfile(String id) {
        return validationProfileDAO.getValidationProfileByID(id);
    }

    @Override
    public void importProfile(ValidationProfile validationProfile) {
        validationProfileDAO.saveValidationProfile(validationProfile);
    }

    @Override
    public List<String> importProfileBatch(InputStream is) {
        Map<String, String> fileContents = FileUtils.extractJsonStringFromZip(is);
        List<String> batchProfileIds = new ArrayList<>();
        for (Map.Entry<String, String> entry : fileContents.entrySet()) {
            try {
                ValidationProfile profile = validationProfileValidator.validate(entry.getValue());
                importProfile(profile);
                batchProfileIds.add(ServiceMessage.VALID_PROFILE.getFormattedValue(profile.getId()));
            } catch (ValidationProfileException e) {
                throw new ValidationProfileException(entry.getKey() + "\n" + e.getMessage());
            }
        }
        return batchProfileIds;
    }

    @Override
    public void deleteProfile(String id) {
        validationProfileDAO.deleteValidationProfile(id);
    }
}
