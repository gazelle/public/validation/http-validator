package net.ihe.gazelle.httpvalidator.application.security;

import net.ihe.gazelle.gazelletokenclient.domain.IdentityToken;
import net.ihe.gazelle.gazelletokenclient.interlay.exceptions.ExpiredTokenException;
import net.ihe.gazelle.gazelletokenclient.interlay.exceptions.InvalidTokenException;

public interface TokenService {
    IdentityToken callGazelleTokenAPI(String tokenRequest) throws InvalidTokenException, ExpiredTokenException;
}
