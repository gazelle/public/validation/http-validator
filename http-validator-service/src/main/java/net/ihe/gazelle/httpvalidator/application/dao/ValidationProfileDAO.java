package net.ihe.gazelle.httpvalidator.application.dao;

import net.ihe.gazelle.httpvalidator.application.config.Preferences;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;

import java.nio.file.Path;
import java.util.List;

public interface ValidationProfileDAO {

    void init();
    Preferences getPreferences();
    List<ProfileMetadata> getProfileMetadata();
    List<ValidationProfile> getAllProfiles();
    ValidationProfile getValidationProfileByID(String id);
    Path getProfilePathById(String id);
    void saveValidationProfile(ValidationProfile validationProfile);
    void deleteValidationProfile(String id);
}
