package net.ihe.gazelle.httpvalidator.application.config;

public interface Preferences {

    String getApplicationUrl();
    String getTokenApiUrl();

    String getValidationProfileRepositoryPath();

    Integer getMaxCacheElements();
}
