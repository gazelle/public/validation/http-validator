package net.ihe.gazelle.it;

import io.restassured.RestAssured;

public class RestConfig {
    static void setGlobalITConfig() {
        RestAssured.baseURI = System.getProperty("it.base.uri", "http://localhost");
        RestAssured.port = Integer.parseInt(System.getProperty("it.base.port", "8480"));
        RestAssured.basePath = "/http-validator/rest";
    }
}
