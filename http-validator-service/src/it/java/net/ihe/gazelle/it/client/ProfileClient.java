package net.ihe.gazelle.it.client;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.FileUtils;

import java.io.File;
import java.util.Objects;

import static io.restassured.RestAssured.given;

public class ProfileClient {

    private static final String MOCK_TOKEN = "GazelleApiKey mock_token";

    public Response getProfiles() {
        return given()
                .header("Authorization", MOCK_TOKEN)
                .when()
                .get("/metadata")
                .then()
                .extract().response();
    }

    public Response postValidProfile() {
        return given()
                .contentType(ContentType.JSON)
                .header("Authorization", MOCK_TOKEN)
                .body(buildValidJsonTestBody())
                .when()
                .post("/validationprofiles")
                .then()
                .extract().response();
    }

    public Response postProfileBatch() {
        return given()
                .multiPart("file", buildBatchTestBody())
                .header("Authorization", MOCK_TOKEN)
                .when()
                .post("/validationprofiles/batch")
                .then()
                .extract().response();
    }

    public Response deleteProfile(String id) {
        return given()
                .header("Authorization", MOCK_TOKEN)
                .when()
                .delete("/validationprofiles/delete?id=" + id)
                .then()
                .extract().response();
    }

    public Response postNotValidProfile() {
        return given()
                .contentType(ContentType.JSON)
                .header("Authorization", MOCK_TOKEN)
                .body(buildNotValidJsonTestBody())
                .when()
                .post("/validationprofiles")
                .then()
                .extract().response();
    }

    private File buildBatchTestBody() {
        return new File(Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource("ValidationProfileBatch.zip")).getPath());
    }

    private String buildValidJsonTestBody() {
        return FileUtils.readFileFromName("ok/ValidationProfile.json");
    }

    private String buildNotValidJsonTestBody() {
        return FileUtils.readFileFromName("ko/ValidationProfile.json");
    }
}
